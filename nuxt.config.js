const laravelNuxt = require("laravel-nuxt");

module.exports = laravelNuxt({
	head: {
		meta: [
			{ charset: 'utf-8' },
    		{ name: 'viewport', content: 'width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' }
		],
    link: [
      {
        rel: 'preload',
        href: '/resources/fonts/Montserrat.woff2',
        as: 'font',
        crossorigin: 'crossorigin',
        type: 'font/woff2',
      }
    ]
	},
  loading: {
    color: '#408fce',
    height: '3px'
  },
  // Options such as mode, srcDir and generate.dir are already handled for you.
  modules: [
    ['nuxt-sass-resources-loader', [
      '@/assets/sass/global.sass'
    ]]
  ],
  css: [
    '@fortawesome/fontawesome-svg-core/styles.css', 'aos/dist/aos.css'
  ],
  vue: {
    config: {
      productionTip: false,
      devtools: true
    }
  },

  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'style', 'font'].includes(type)
      }
    }

  },

  build: {
    optimization: {
      splitChunks: {
        name: true
      }
    }
  },


plugins: ['~plugins/vue-js-modal', '~/plugins/fontawesome.js', '~/plugins/aos.js'],
  srcDir: 'client/',
});
