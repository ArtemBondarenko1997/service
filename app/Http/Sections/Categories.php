<?php

namespace App\Http\Sections;

use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\Category;
use App\Models\Module;
use App\Models\Service;
use App\Services\FileServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Http\UploadedFile;
use SleepingOwl\Admin\Display\Tree\OrderTreeType;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;

/**
 * Class Categories
 *
 * @property Category $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Categories extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = "Категории";

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {
    $this->addToNavigation()->setPriority(100)->setIcon('fa fa-list-ul');
  }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   */
  public function onDisplay($payload = [])
  {
    return AdminDisplay::tree(OrderTreeType::class)->setValue('name');
  }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   * @throws \Exception
   */
  public function onEdit($id = null, $payload = [])
  {

    $ignore = ($id) ? ',' . $id : '';
    $form = AdminForm::form()->addElement(
      AdminFormElement::columns()
        ->addColumn([AdminFormElement::text('name', 'Название')
          ->setValidationRules(['name' => 'unique:categories,name' . $ignore])
          ->addValidationMessage('unique', 'Такая категория уже существует')
          ->required(),], 6)
        ->addColumn([AdminFormElement::text('title', 'Заголовок'),], 12)
        ->addColumn([AdminFormElement::wysiwyg('description', 'Описание'),], 12)
        ->addColumn([AdminFormElement::image('icon', 'Icon')
          ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])
          ->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
            /** @var FileServices $fileName */
            $fileName = FileServices::generateFileName($file, $file->getClientOriginalName(), Category::FOLDER);
            return $fileName;
          })
          ->setUploadSettings([
            'orientate' => [],
            'resize' => [256, 256, function ($constraint) {
              $constraint->upsize();
            }],
          ])
          ->setUploadPath(
            function (UploadedFile $file) {
              $folder = Category::FOLDER;
              if (!File::exists('images/' . $folder)) {
                File::makeDirectory('images/' . $folder, 0777, true, true);
              }
              return 'images/' . $folder;
            }),], 6)
        ->addColumn([AdminFormElement::image('logo', 'Главное Фото')
//          ->setUploadSettings([
//            'orientate' => [],
//            'resize' => [256, 256, function ($constraint) {
//              $constraint->upsize();
//            }],
//          ])
          ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
            /** @var FileServices $fileName */
            $fileName = FileServices::generateFileName($file, $file->getClientOriginalName(), Category::FOLDER);
            return $fileName;
          })
          ->setUploadPath(
            function (UploadedFile $file) {
              $folder = Category::FOLDER;
              if (!File::exists('images/' . $folder)) {
                File::makeDirectory('images/' . $folder, 0777, true, true);
              }
              return 'images/' . $folder;
            }),], 6)
        ->addColumn([AdminFormElement::hidden('slug'),], 12)
    );

    $formModule = AdminForm::form()->addElement(
      AdminFormElement::hasMany('modulePivot', [
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::select('module_id', 'Модуль', Module::all()->pluck('name', 'id')->toArray())
            ->required(),], 5)
          ->addColumn([AdminFormElement::select('service_id', 'Услуга', Service::whereNotNull('parent_id')->get()->pluck('name', 'id')->toArray())->required(),], 5)
      ])
    );

    $tabs = AdminDisplay::tabbed();
    $tabs->appendTab($form, 'Основное');

    if ($id) {
      $tabs->appendTab($formModule, 'Модули');
    } else {
      $tabs->appendTab(AdminForm::form()->addElement(
        AdminFormElement::html(function (Category $category) {
          return '<h2>Чтоб добавить услугу и модуль, сохраните информацию во вкладке "Основное"!</h2>';
        })), 'Модули');
    }

    return $tabs;
  }

  /**
   * @param array $payload
   * @return FormInterface
   * @throws \Exception
   */
  public function onCreate($payload = [])
  {
    return $this->onEdit(null, $payload);
  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    return true;
  }
}
