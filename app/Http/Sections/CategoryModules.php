<?php

namespace App\Http\Sections;

use AdminColumnFilter;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use App\Models\Category;
use App\Models\CategoryModule;
use App\Models\Module;
use App\Models\Service;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Exceptions\FilterOperatorException;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;

/**
 * Class CategoryModules
 *
 * @property CategoryModule $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class CategoryModules extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = "Неисправности";

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {
    $this->addToNavigation()->setPriority(100)->setIcon('fa fa-tasks');
  }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   * @throws FilterOperatorException
   */
  public function onDisplay($payload = [])
  {
    $display = AdminDisplay::datatablesAsync()
      ->setHtmlAttribute('class', 'table-primary')
      ->setColumns(
        AdminColumn::text('id', '#')->setWidth('30px'),
        AdminColumn::text('category.name', 'Категория'),
        AdminColumn::image('module.icon', 'Icon'),
        AdminColumn::text('module.name', 'Модуль'),
        AdminColumn::text('service.name', 'Услуга')
      )
      ->setOrder([0, 'ASC'])
      ->paginate(20);

    $display->setColumnFilters([
      AdminColumnFilter::text()
        ->setPlaceholder('ID')
        ->setOperator('contains')
        ->setHtmlAttribute('style', 'width:100px'),
      AdminColumnFilter::select(new Category())
        ->setDisplay('name')
        ->setPlaceholder('Категория')
        ->setColumnName('category_id'),
      null,
      AdminColumnFilter::select(new Service())
        ->setDisplay('name')
        ->setPlaceholder('Услуга')
        ->setColumnName('service_id'),
      AdminColumnFilter::select(new Module())
        ->setDisplay('name')
        ->setPlaceholder('Модуль')
        ->setColumnName('module_id'),
    ])->setPlacement('table.header');

    return $display;
  }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   * @throws \Exception
   */
  public function onEdit($id = null, $payload = [])
  {
    $form = AdminForm::form()->addElement(
      AdminFormElement::columns()
        ->addColumn([AdminFormElement::select('category_id', 'Категория', Category::all()->pluck('name', 'id')->toArray())->required(),], 12)
        ->addColumn([AdminFormElement::select('module_id', 'Модуль', Module::all()->pluck('name', 'id')->toArray())
          ->required(),], 12)
        ->addColumn([AdminFormElement::select('service_id', 'Услуга', Service::whereNotNull('parent_id')->get()->pluck('name', 'id')->toArray())->required(),], 12)

    );

    return $form;
  }

  /**
   * @param array $payload
   * @return FormInterface
   * @throws \Exception
   */
  public function onCreate($payload = [])
  {
    return $this->onEdit(null, $payload);
  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    return true;
  }
}
