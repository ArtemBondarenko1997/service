<?php

namespace App\Http\Sections;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use App\Models\Schedule;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;

/**
 * Class Schedules
 *
 * @property \App\Models\Schedule $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Schedules extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'График работы';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-clock');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
      $display = AdminDisplay::datatables();
      $display->setApply(function ($query) {
        $query->orderBy('day', 'asc');
      });
      $display->setHtmlAttribute('class', 'table-primary')
        ->setColumns([
          AdminColumn::link('id', '#')->setWidth('30px'),
          AdminColumn::link('day_name', 'День'),
//          AdminColumn::text('work', 'Время'),
          \AdminColumnEditable::text('work', 'Время'),
//          AdminColumn::datetime('created_at', 'Дата создания')->setFormat('Y m d'),
//          AdminColumn::datetime('updated_at', 'Дата изменения')->setFormat('Y m d')
        ]);

      return $display;
    }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   * @throws \Exception
   */
    public function onEdit($id = null, $payload = [])
    {

      $day = Schedule::find($id);
      $day_name = $day->day_name ?? '';
      return AdminForm::form()->addElement(
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::text('work', 'Время работы - ' . $day_name)->required(),], 12)
      );
    }


  /**
   * @param Model $model
   * @return bool
   */
    public function isDeletable(Model $model)
    {
        return false;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
