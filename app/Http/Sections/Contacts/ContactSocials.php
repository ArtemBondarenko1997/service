<?php

namespace App\Http\Sections\Contacts;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Display\Tree\OrderTreeType;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;

/**
 * Class Contacts
 *
 * @property Contact $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class ContactSocials extends Contacts implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title="Соц. сети";

  /**
   * @var string
   */
  protected $alias = 'contacts-social';

}
