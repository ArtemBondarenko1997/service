<?php

namespace App\Http\Sections\Contacts;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Display\Tree\OrderTreeType;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;

/**
 * Class Contacts
 *
 * @property Contact $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Contacts extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title="Все контакты";

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {
//    $this->addToNavigation()->setPriority(100)->setIcon('fa fa-lightbulb-o');
  }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   */
  public function onDisplay($payload = [])
  {
    $display = AdminDisplay::datatablesAsync();

    $display->setApply(function ($query) {
      $type = request()->get('slug', null);
      $query->orderBy('order', 'asc');
      ($type) ? $query->where('sys_type', '=', $type) : null;
    });

    $display->setHtmlAttribute('class', 'table-primary')
      ->setColumns([
        AdminColumn::text('id', '#')->setWidth('30px'),
        AdminColumn::text('name', 'Название'),
        AdminColumn::text('value', 'Значение'),
        AdminColumn::custom('Публикация', function (Contact $contact) {
          return $contact->is_publish ? '<i class="fa fa-check"></i>' : '<i class="fa fa-minus"></i>';
        })
      ])->paginate(20);

    $tree = AdminDisplay::tree(OrderTreeType::class);
    $tree->setApply(function ($query) {
      $type = request()->get('slug', null);
      $query->orderBy('order', 'asc');
      ($type) ? $query->where('sys_type', '=', $type) : null;
    });

    $tree->setValue('name');
    $tabs = AdminDisplay::tabbed();
    $tabs->appendTab($display, 'Основное');
    $tabs->appendTab($tree, 'Сортировка');
    return $tabs;
  }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   */
  public function onEdit($id = null, $payload = [])
  {
    return AdminForm::panel()->addBody([
      AdminFormElement::checkbox('is_publish', 'Публикация'),
      AdminFormElement::text('name', 'Название')->setReadonly(true),
      AdminFormElement::text('value', 'Значение')->required(),
    ]);
  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    return false;
  }

}
