<?php

namespace App\Http\Sections;

use AdminDisplay;
use AdminColumn;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use App\Models\Image;
use App\Services\FileServices;
use Illuminate\Http\UploadedFile;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Exceptions\Form\Element\SelectException;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use Illuminate\Support\Facades\File;

/**
 * Class Images
 *
 * @property \App\Models\Image $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Images extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Изображения';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(900)->setIcon('fa fa-images');
    }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   * @throws \SleepingOwl\Admin\Exceptions\FilterOperatorException
   */
    public function onDisplay($payload = [])
    {
      $display = AdminDisplay::datatablesAsync();

      $display->setHtmlAttribute('class', 'table-primary')
        ->setColumns([
          AdminColumn::link('id', '#')->setWidth('30px'),
          AdminColumn::image('path', 'Изображение'),
          AdminColumn::link('name', 'Название'),
          AdminColumn::datetime('created_at', 'Дата создания')->setFormat('Y m d'),
          AdminColumn::datetime('updated_at', 'Дата изменения')->setFormat('Y m d')
        ])->paginate(20);

      return $display;
    }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return void
   * @throws \Exception
   */
    public function onEdit($id = null, $payload = [])
    {

      $image= Image::find($id);
      $resolution = $image->getResolution();

      return AdminForm::form()->addElement(
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::text('name', 'Описание')->setReadonly(true),], 12)
          ->addColumn([AdminFormElement::image('path', 'Изображение')
            ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])
            ->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id, $image) {
              /** @var FileServices $fileName */
              $extension = '.' . $file->getClientOriginalExtension() ?? 'jpg';
              $fileName = FileServices::generateFileName($file, $image->type . $extension, Image::FOLDER);
              return $fileName;
            })
            ->setUploadSettings([
              'orientate' => [],
              'resize' => [$resolution['width'], $resolution['height'], function ($constraint) {
                $constraint->upsize();
              }],
            ])
            ->setUploadPath(
              function (UploadedFile $file) {
                $folder = Image::FOLDER;
                if (!File::exists('images/' . $folder)) {
                  File::makeDirectory('images/' . $folder, 0777, true, true);
                }
                return 'images/' . $folder;
              }),], 12)
      );
    }


  /**
   * @param Model $model
   * @return bool
   */
    public function isDeletable(Model $model)
    {
        return false;
    }

  /**
   * @return bool
   */
  public function isCreatable()
  {
    return false;
  }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
