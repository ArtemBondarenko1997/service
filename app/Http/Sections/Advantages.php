<?php

namespace App\Http\Sections;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use App\Models\Advantage;
use App\Services\FileServices;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Display\Tree\OrderTreeType;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;

/**
 * Class Advantages
 *
 * @property \App\Models\Advantage $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Advantages extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Наши преимущества';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-thumbs-up');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
      return AdminDisplay::tree(OrderTreeType::class)->setValue('title');
    }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   * @throws \Exception
   */
    public function onEdit($id = null, $payload = [])
    {

      $form = AdminForm::form()->addElement(
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::checkbox('publish', 'Публикация'),], 12)
          ->addColumn([AdminFormElement::text('title', 'Заголовок')->required(),], 12)
          ->addColumn([AdminFormElement::wysiwyg('description', 'Описание')->required(),], 12)
          ->addColumn([AdminFormElement::image('image', 'Фото')
            ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])
            ->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
              /** @var FileServices $fileName */
              $fileName = FileServices::generateFileName($file, $file->getClientOriginalName(), Advantage::FOLDER);
              return $fileName;
            })
            ->setUploadPath(
              function (UploadedFile $file) {
                $folder = Advantage::FOLDER;
                if (!File::exists('images/' . $folder)) {
                  File::makeDirectory('images/' . $folder, 0777, true, true);
                }
                return 'images/' . $folder;
              }),], 6)

      );

      return $form;
    }

  /**
   * @param array $payload
   * @return FormInterface
   * @throws \Exception
   */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
