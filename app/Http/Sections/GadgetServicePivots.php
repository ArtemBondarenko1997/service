<?php

namespace App\Http\Sections;

use App\Models\Gadget;
use App\Models\GadgetServicePivot;
use App\Models\Service;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Display\Tree\OrderTreeType;
use SleepingOwl\Admin\Exceptions\FilterOperatorException;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;

/**
 * Class GadgetServicePivots
 *
 * @property GadgetServicePivot $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class GadgetServicePivots extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = 'Услуги и категории';

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {
//    $this->addToNavigation()->setPriority(100)->setIcon('fa fa-lightbulb-o');
  }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   * @throws FilterOperatorException
   */
  public function onDisplay($payload = [])
  {
    if (request()->get('data', null)) {
      return AdminDisplay::tree()->setValue('service_id');
    }

    $display = AdminDisplay::datatablesAsync();

    $display->setHtmlAttribute('class', 'table-primary')
      ->setColumns([
        AdminColumn::link('id', '#')->setWidth('30px'),
        AdminColumn::link('gadget.model', 'Модель'),
        AdminColumn::custom('Категория', function (GadgetServicePivot $gadgetServicePivot) {
          return $gadgetServicePivot->parent_id ? '' : $gadgetServicePivot->service_name;
        }),
        AdminColumn::custom('Услуга', function (GadgetServicePivot $gadgetServicePivot) {
          return $gadgetServicePivot->parent_id ? $gadgetServicePivot->service_name :'';
        }),
        AdminColumn::custom('От', function (GadgetServicePivot $gadgetServicePivot) {
          return $gadgetServicePivot->is_from ? 'от' : '';
        })->setWidth('50px'),
        AdminColumn::text('price', 'Цена, грн.'),
        AdminColumn::text('time', 'Время')
      ])
      ->setOrder([1, 'ASC'])
      ->paginate(20);


    $servicesSelect = Service::whereNotNull('parent_id')->get()->pluck('name', 'id')->toArray();
    $parentServicesSelect = Service::whereNull('parent_id')->get()->pluck('name', 'id')->toArray();
    $display->setColumnFilters([
      null,
      AdminColumnFilter::select(new Gadget())
        ->setDisplay('model')
        ->setPlaceholder('Модель')
        ->setColumnName('gadget_id'),
      AdminColumnFilter::select($parentServicesSelect)
        ->setDisplay('name')
        ->setPlaceholder('Категория')
        ->setColumnName('service_id'),
      AdminColumnFilter::select($servicesSelect)
        ->setDisplay('name')
        ->setPlaceholder('Услуга')
        ->setColumnName('service_id'),
      AdminColumnFilter::select([0 => 'Нет', 1 => 'Да'])
        ->setPlaceholder('От')
        ->setColumnName('is_from'),
      AdminColumnFilter::text()->setPlaceholder('Цена, грн.')->setOperator('equal'),
      AdminColumnFilter::text()->setPlaceholder('Время')->setOperator('contains'),
    ])->setPlacement('table.header');
    $display->setView('display.menu_items_tree');
    return $display;
  }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   * @throws Exception
   */
  public function onEdit($id = null, $payload = [])
  {

    $model= GadgetServicePivot::find($id);
    $this->setGadgetToModel();
    if($model->parent_id){
      $parentServices = GadgetServicePivot::where('gadget_id', '=', $model->gadget_id)->whereNull('parent_id')->get();
      $form = AdminForm::form()->addElement(
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::select('parent_id', 'Категория')
            ->setModelForOptions(GadgetServicePivot::class, 'service_name')
            ->setLoadOptionsQueryPreparer(function($item, $query) use ($parentServices) {
              return $query->whereIn('id', $parentServices->pluck('id')->toArray());
            })->required(),], 6)

          ->addColumn([AdminFormElement::dependentselect('service_id', 'Услуга', ['parent_id'])
            ->setModelForOptions(Service::class, 'name')
            ->setDataUrl('/api/admin/gadget-dependent')->required(),], 6)

          ->addColumn([AdminFormElement::text('time', 'Гарантия'),], 12)
          ->addColumn([AdminFormElement::checkbox('is_from', 'от'),], 1)
          ->addColumn([AdminFormElement::number('old_price', 'Старая цена'),], 2)
          ->addColumn([AdminFormElement::number('price', 'Цена')->required(),], 2)

      );
    }else{
      $form = AdminForm::form()->addElement(
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::select('service_id', 'Категория', Service::whereNull('parent_id')->get()->pluck('name', 'id')->toArray())->required(),], 12)
      );
    }

    return $form;
  }

  public function onCreate($payload = [])
  {
    $type = request()->get('type');
    $this->setGadgetToModel();

    if($type === 'category'){
      $form = AdminForm::form()->addElement(
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::select('gadget_id', 'Девайс',Gadget::all()->pluck('model', 'id')->toArray())->required(),], 12)
          ->addColumn([AdminFormElement::select('service_id', 'Категория', $this->getGadgetCategory())->required(),], 12)
      );
    }else{
      $form = AdminForm::form()->addElement(
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::select('gadget_id', 'Девайс',Gadget::all()->pluck('model', 'id')->toArray())->required(),], 6)

          ->addColumn([AdminFormElement::dependentselect('parent_id', 'Категория',  ['gadget_id'])
            ->setModelForOptions(GadgetServicePivot::class, 'service_name')
            ->setLoadOptionsQueryPreparer(function($item, $query) {
              return $query->where('gadget_id', '=', $item->getDependValue('gadget_id'))->whereNull('parent_id');
            })->required(),], 6)

          ->addColumn([AdminFormElement::dependentselect('service_id', 'Услуга', ['parent_id'])
            ->setModelForOptions(Service::class, 'name')
            ->setDataUrl('/api/admin/gadget-dependent')->required(),], 6)

          ->addColumn([AdminFormElement::text('time', 'Гарантия'),], 6)
          ->addColumn([AdminFormElement::checkbox('is_from', 'от'),], 1)
          ->addColumn([AdminFormElement::number('old_price', 'Старая цена'),], 2)
          ->addColumn([AdminFormElement::number('price', 'Цена')->required(),], 2)

      );

    }

    $form->setAction('?type=' . $type);

    $form->getButtons()->setButtons([
      'save_and_close'  => (new SaveAndClose())->setGroupElements([
        'save_and_create' => new SaveAndCreate(),
      ]),
      'save'   => new Save(),
    ]);

    return $form;

  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    if (\Request::route()->getName() === "admin.model.edit") {
//      return false;
    }
    return true;
  }

  /**
   * @return bool
   */
  public function isCreatable()
  {
    return true;
  }


  protected function setGadgetToModel()
  {
    if(request()->get('gadget_id', null)){
      $t = $this->getModel();
      $t->gadget_id = request()->get('gadget_id', null);
      $this->setModelValue($t);
    }
  }

  /**
   * @return mixed
   */
  protected function getGadgetCategory()
  {
    if(request()->get('gadget_id', null)){
      $isset_category = GadgetServicePivot::where('gadget_id', request()->get('gadget_id'))->whereNull('parent_id')->get()->pluck('service_id');
      return Service::whereNull('parent_id')->whereNotIn('id', $isset_category)->get()->pluck('name', 'id')->toArray();
    }

    return Service::whereNull('parent_id')->get()->pluck('name', 'id')->toArray();
  }
}
