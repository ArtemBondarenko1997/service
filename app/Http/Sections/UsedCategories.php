<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;

/**
 * Class UsedCategories
 *
 * @property \App\Models\UsedCategory $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class UsedCategories extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = "Категории";

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {}

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   */
  public function onDisplay($payload = [])
  {
    return AdminDisplay::datatablesAsync()
      ->setHtmlAttribute('class', 'table-primary')
      ->setColumns([
        AdminColumn::text('id', '#')->setWidth('30px'),
        AdminColumn::text('name', 'Название')
      ])->setOrder([0, 'ASC'])
      ->paginate(20);
  }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   */
  public function onEdit($id = null, $payload = [])
  {
    return AdminForm::panel()->addBody([
      AdminFormElement::text('name', 'Название')->required()->unique(),
      AdminFormElement::hidden('slug'),
    ]);
  }

  /**
   * @return FormInterface
   */
  public function onCreate($payload = [])
  {
    return $this->onEdit(null, $payload);
  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    return true;
  }
}
