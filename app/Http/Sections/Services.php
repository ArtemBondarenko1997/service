<?php

namespace App\Http\Sections;

use App\Models\Service;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;

/**
 * Class Services
 *
 * @property Service $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Services extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title="Услуги";

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
//        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-lightbulb-o');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
//        $display = AdminDisplay::datatablesAsync()
//            ->setHtmlAttribute('class', 'table-primary')
//            ->setColumns(
//                AdminColumn::link('id', '#')->setWidth('30px'),
//                AdminColumn::link('name', 'Услуги'),
//              AdminColumn::datetime('created_at', 'Дата создания')->setFormat('Y m d'),
//              AdminColumn::datetime('updated_at', 'Дата изменения')->setFormat('Y m d')
//            )
//            ->setOrder([0, 'ASC'])
//            ->paginate(20);
//
//      $display->setColumnFilters([
//        null,
////        AdminColumnFilter::text()->setPlaceholder('name')->setOperator('contains'),
//        AdminColumnFilter::select(new Service)
//          ->setDisplay('name')
//          ->setPlaceholder('Услуга')
//          ->setColumnName('id')
//      ])->setPlacement('table.header');
//
//        return $display;
      return AdminDisplay::tree()->setValue('name');
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
      $ignore = ($id) ? ',' . $id : '';
      $parents = Service::whereNull('parent_id')->get()->pluck('name', 'id')->toArray();
        $form = AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Услуга')
              ->setValidationRules(['name' => 'unique:services,name' . $ignore])
              ->addValidationMessage('unique', 'Такая услуга уже существует')
              ->required(),
            AdminFormElement::select('parent_id', 'Категория (если не назначать категорию, то созданная услуга будет категорией)', $parents),
        ]);

        return $form;
    }

  /**
   * @param array $payload
   * @return FormInterface
   */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

  /**
   * @param Model $model
   * @return bool
   */
    public function isDeletable(Model $model)
    {
        return true;
    }
}
