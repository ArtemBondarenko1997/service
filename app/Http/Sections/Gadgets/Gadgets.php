<?php

namespace App\Http\Sections\Gadgets;

use AdminSection;
use App\Models\Category;
use App\Models\Gadget;
use App\Models\GadgetServicePivot;
use App\Models\Service;
use App\Services\FileServices;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use SleepingOwl\Admin\Display\DisplayTabbed;
use SleepingOwl\Admin\Display\Tree\OrderTreeType;
use SleepingOwl\Admin\Exceptions\FilterOperatorException;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;

/**
 * Class Gadgets
 *
 * @property Gadget $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Gadgets extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = "Все девайсы";

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {
//    $this->addToNavigation()->setPriority(100)->setIcon('fa fa-tablet');
  }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   * @throws FilterOperatorException
   */
  public function onDisplay($payload = [])
  {
    $display = AdminDisplay::datatablesAsync();

    $display->setApply(function ($query) {
      $category = request()->get('category', null);
      $query->orderBy('order', 'asc');
      ($category) ? $query->where('category_id', '=', $category) : null;
    });
    $display->setHtmlAttribute('class', 'table-primary')
      ->setColumns([
        AdminColumn::link('id', '#')->setWidth('50px'),
        AdminColumn::image('logo', 'Фото'),
        AdminColumn::link('category.name', 'Категория'),
        AdminColumn::text('model', 'Модель'),
        AdminColumn::text('title', 'Заголовок'),
      ])->paginate(20);

    $display->setColumnFilters([
      null,
      null,
      AdminColumnFilter::select(new Category())
        ->setDisplay('name')
        ->setPlaceholder('Категория')
        ->setColumnName('category_id'),
      AdminColumnFilter::text()->setPlaceholder('Модель')->setOperator('contains'),
    ])->setPlacement('table.header');

    $tree = AdminDisplay::tree(OrderTreeType::class);
    $tree->setApply(function ($query) {
      $category = request()->get('category', null);
      $query->orderBy('order', 'asc');
      ($category) ? $query->where('category_id', '=', $category) : null;
    });
    $tree->setValue('model');
    $tabs = AdminDisplay::tabbed();
    $tabs->appendTab($display, 'Основное');
    $tabs->appendTab($tree->setValue('model'), 'Сортировка');
    return $tabs;
  }


  /**
   * @param null $id
   * @param array $payload
   * @return DisplayTabbed
   * @throws Exception
   */
  public function onEdit($id = null, $payload = [])
  {
    $ignore = ($id) ? ',' . $id : '';

    $form = AdminForm::form()->addElement(
      AdminFormElement::columns()
        ->addColumn([AdminFormElement::select('category_id', 'Категория', Category::all()->pluck('name', 'id')->toArray())->required(),], 6)
        ->addColumn([AdminFormElement::text('model', 'Модель')
          ->setValidationRules(['model' => 'unique:gadgets,model' . $ignore])
          ->addValidationMessage('unique', 'Такая модель уже существует')
          ->required(),], 6)
        ->addColumn([AdminFormElement::text('title', 'Заголовок'),], 12)
        ->addColumn([AdminFormElement::wysiwyg('description', 'Описание'),], 12)
        ->addColumn([AdminFormElement::wysiwyg('article', 'Статья'),], 12)
        ->addColumn([AdminFormElement::image('icon', 'Icon')
          ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
            /** @var FileServices $fileName */
            $fileName = FileServices::generateFileName($file, $file->getClientOriginalName(), Category::FOLDER);
            return $fileName;
          })
          ->setUploadPath(
            function (UploadedFile $file) {
              $folder = Gadget::FOLDER;
              if (!File::exists('images/' . $folder)) {
                File::makeDirectory('images/' . $folder, 0777, true, true);
              }
              return 'images/' . $folder;
            }),], 6)
        ->addColumn([AdminFormElement::image('logo', 'Главное Фото')
          ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
            /** @var FileServices $fileName */
            $fileName = FileServices::generateFileName($file, $file->getClientOriginalName(), Category::FOLDER);
            return $fileName;
          })
          ->setUploadPath(
            function (UploadedFile $file) {
              $folder = Gadget::FOLDER;
              if (!File::exists('images/' . $folder)) {
                File::makeDirectory('images/' . $folder, 0777, true, true);
              }
              return 'images/' . $folder;
            }),], 6)
        ->addColumn([AdminFormElement::hidden('slug'),], 12)
    );

    $parentServices = GadgetServicePivot::select('id')->where('gadget_id', '=', $id)->whereNull('parent_id')->get();

    $formParentServices = AdminForm::form()->addElement(
      AdminFormElement::hasMany('pivotParent', [
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::select('service_id', 'Категория', Service::whereNull('parent_id')->get()->pluck('name', 'id')->toArray())->required(),], 6)
      ])
    );

    $tabs = AdminDisplay::tabbed();
    $tabs->appendTab($form, 'Основное');
    if ($id) {
      $tabs->appendTab($formParentServices, 'Категории услуг');
    } else {
      $tabs->appendTab(AdminForm::form()->addElement(
        AdminFormElement::html(function (Gadget $gadget) {
          return '<h2>Чтоб добавить категорию услуг, сохраните информацию во вкладке "Основное"!</h2>';
        })), 'Категории услуг');
    }


    if (!is_null($id)) {
      $order = AdminDisplay::tree()
        ->setModelClass(GadgetServicePivot::class)
        ->setApply(function ($query) use ($id) {
          $query->where('gadget_id', $id);
        })->setValue('service_name')
        ->setView('display.device_tree')
        ->setParameter('alias', $this->alias)
        ->setParameter('gadget_id', $id);
      $tabs->appendTab($order, 'Услуги');

    }

    return $tabs;
  }


  /**
   * @param array $payload
   * @return DisplayTabbed
   * @throws Exception
   */
  public function onCreate($payload = [])
  {
    return $this->onEdit(null, $payload);
  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    return true;
  }
}
