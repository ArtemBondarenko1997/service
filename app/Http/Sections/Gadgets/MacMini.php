<?php

namespace App\Http\Sections\Gadgets;

use App\Models\Gadget;

/**
 * Class Gadgets
 *
 * @property Gadget $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class MacMini extends Gadgets
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = "Все девайсы";

  /**
   * @var string
   */
  protected $alias = 'gadget-mac-mini';

}
