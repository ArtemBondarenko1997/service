<?php

namespace App\Http\Sections;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use App\Models\Slider;
use App\Services\FileServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;

/**
 * Class Sliders
 *
 * @property Slider $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Sliders extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = "Слайдер";

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {
    $this->addToNavigation()->setPriority(100)->setIcon('fa fa-camera-retro');
  }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   */
  public function onDisplay($payload = [])
  {
    $display = AdminDisplay::datatablesAsync();

    $display->setApply(function ($query) {
      $query->orderBy('order', 'asc');
    });

    $display->setHtmlAttribute('class', 'table-primary')
      ->setColumns([
        AdminColumn::link('id', '#')->setWidth('30px'),
        AdminColumn::image('path', 'Слайд'),
        AdminColumn::order()->setLabel('Сортировка'),
        AdminColumn::datetime('created_at', 'Дата создания')->setFormat('Y m d'),
        AdminColumn::datetime('updated_at', 'Дата изменения')->setFormat('Y m d')
      ])->paginate(20);

    return $display;
  }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   */
  public function onEdit($id = null, $payload = [])
  {
    return AdminForm::panel()->addBody([
      AdminFormElement::image('path', 'Слайд')
        ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
          /** @var FileServices $fileName */
          $fileName = FileServices::generateFileName($file, $file->getClientOriginalName(), Slider::FOLDER);
          return $fileName;
        })
        ->setUploadPath(
          function (UploadedFile $file) {
            $folder = Slider::FOLDER;
            if (!File::exists('images/' . $folder)) {
              File::makeDirectory('images/' . $folder, 0777, true, true);
            }
            return 'images/' . $folder;
          })->required(),
      AdminFormElement::text('order', 'Сортиповка'),
      AdminFormElement::text('title', 'Заголовок')
    ]);
  }

  /**
   * @param array $payload
   * @return FormInterface
   */
  public function onCreate($payload = [])
  {
    return $this->onEdit(null, $payload);
  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    return true;
  }
}
