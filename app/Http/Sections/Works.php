<?php

namespace App\Http\Sections;

use AdminColumnFilter;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use App\Models\Gadget;
use App\Models\Service;
use Illuminate\Support\Facades\File;
use Illuminate\Http\UploadedFile;
use App\Models\Work;
use App\Services\FileServices;
use Exception;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Exceptions\FilterOperatorException;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;

/**
 * Class Works
 *
 * @property Work $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Works extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = "Наши работы";

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {
    $this->addToNavigation()->setPriority(100)->setIcon('fa fa-wrench');
  }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   * @throws FilterOperatorException
   */
  public function onDisplay($payload = [])
  {
    $display = AdminDisplay::datatablesAsync()
      ->setHtmlAttribute('class', 'table-primary')
      ->setColumns([
        AdminColumn::link('id', '#')->setWidth('30px'),
        AdminColumn::image('before', 'До'),
        AdminColumn::image('after', 'После'),
//        AdminColumn::text('service', 'Услуга'),
        AdminColumn::text('price', 'Цена'),
        AdminColumn::text('time', 'Время'),
        AdminColumn::text('title', 'Заголовок')
      ])
      ->setOrder([0, 'ASC'])
      ->paginate(20);

    $display->setColumnFilters([
      AdminColumnFilter::text()->setPlaceholder('ID')
        ->setOperator('contains')
        ->setHtmlAttribute('style', 'width: 100px'),
      null,
      null,
//      AdminColumnFilter::text()->setPlaceholder('Услуга')->setOperator('contains'),
      AdminColumnFilter::text()->setPlaceholder('Цена')->setOperator('contains'),
      AdminColumnFilter::text()->setPlaceholder('Время')->setOperator('contains'),
      AdminColumnFilter::text()->setPlaceholder('Заголовок')->setOperator('contains'),
    ])->setPlacement('table.header');
    return $display;
  }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   * @throws Exception
   */
  public function onEdit($id = null, $payload = [])
  {
    $form = AdminForm::form()->addElement(
      AdminFormElement::columns()
        ->addColumn([AdminFormElement::text('title', 'Заголовок'),], 6)
        ->addColumn([AdminFormElement::text('price', 'Цена'),], 3)
        ->addColumn([AdminFormElement::text('time', 'Время'),], 3)
        ->addColumn([AdminFormElement::select('gadget_slug', 'Ссылка на', Gadget::all()
          ->pluck('model', 'slug')
          ->toArray()),], 12)
//        ->addColumn([AdminFormElement::select('service', 'Услуга', Service::whereNotNull('parent_id')->get()
//          ->pluck('name', 'name')
//          ->toArray()),], 6)
        ->addColumn([AdminFormElement::wysiwyg('description', 'Описание'),], 12)
        ->addColumn([AdminFormElement::image('before', 'До')
          ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])
          ->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
            /** @var FileServices $fileName */
            $fileName = FileServices::generateFileName($file, $file->getClientOriginalName(), Work::FOLDER);
            return $fileName;
          })->setUploadSettings([
            'orientate' => [],
            'resize' => [360, 470, function ($constraint) {
              $constraint->upsize();
            }],
          ])
          ->setUploadPath(
            function (UploadedFile $file) {
              $folder = Work::FOLDER;
              if (!File::exists('images/' . $folder)) {
                File::makeDirectory('images/' . $folder, 0777, true, true);
              }
              return 'images/' . $folder;
            })->required(),], 6)
        ->addColumn([AdminFormElement::image('after', 'После')
          ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])
          ->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
            /** @var FileServices $fileName */
            $fileName = FileServices::generateFileName($file, $file->getClientOriginalName(), Work::FOLDER);
            return $fileName;
          })->setUploadSettings([
            'orientate' => [],
            'resize' => [360, 470, function ($constraint) {
              $constraint->upsize();
            }],
          ])->setUploadPath(
            function (UploadedFile $file) {
              $folder = Work::FOLDER;
              if (!File::exists('images/' . $folder)) {
                File::makeDirectory('images/' . $folder, 0777, true, true);
              }
              return 'images/' . $folder;
            })->required(),], 6)


    );

    return $form;
  }

  /**
   * @param array $payload
   * @return FormInterface
   * @throws Exception
   */
  public function onCreate($payload = [])
  {
    return $this->onEdit(null, $payload);
  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    return true;
  }
}
