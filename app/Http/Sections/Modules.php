<?php

namespace App\Http\Sections;

use AdminColumnFilter;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use App\Services\FileServices;
use Illuminate\Support\Facades\File;
use Illuminate\Http\UploadedFile;
use App\Models\Module;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Exceptions\FilterOperatorException;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;

/**
 * Class Modules
 *
 * @property Module $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Modules extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = "Модули";

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
//        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-lightbulb-o');
    }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   * @throws FilterOperatorException
   */
    public function onDisplay($payload = [])
    {
        $display = AdminDisplay::datatablesAsync()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::link('id', '#')->setWidth('30px'),
                AdminColumn::image('icon', 'Icon'),
                AdminColumn::link('name', 'Название')
            )
            ->setOrder([0, 'ASC'])
            ->paginate(20);

      $display->setColumnFilters([
        AdminColumnFilter::text()->setPlaceholder('ID')
          ->setOperator('contains')
          ->setHtmlAttribute('style', 'width: 100px'),
        null,
        AdminColumnFilter::text()->setPlaceholder('Название')->setOperator('contains'),
      ])->setPlacement('table.header');

        return $display;
    }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   * @throws \Exception
   */
    public function onEdit($id = null, $payload = [])
    {
      $ignore = ($id) ? ',' . $id : '';
      $form = AdminForm::form()->addElement(
        AdminFormElement::columns()
          ->addColumn([AdminFormElement::text('name', 'Название')
            ->setValidationRules(['name' => 'unique:modules,name' . $ignore])
            ->addValidationMessage('unique', 'Такая запись уже существует')
            ->required()
            ,], 12)
          ->addColumn([AdminFormElement::image('icon', 'Icon')
            ->setValidationRules(['icon' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])
            ->setUploadSettings([
              'orientate' => [],
              'resize' => [256, 256, function ($constraint) {
                $constraint->upsize();
              }],
            ])
            ->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
            /** @var FileServices $fileName */
            $fileName = FileServices::generateFileName($file, $file->getClientOriginalName(), Module::FOLDER);
            return $fileName;
          })->setUploadPath(
            function (UploadedFile $file) {
              $folder = Module::FOLDER;
              if (!File::exists('images/' . $folder)) {
                File::makeDirectory('images/' . $folder, 0777, true, true);
              }
              return 'images/' . $folder;
            })->required(),], 12)
      );

      return $form;
    }

  /**
   * @param array $payload
   * @return FormInterface
   * @throws \Exception
   */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

  /**
   * @param Model $model
   * @return bool
   */
    public function isDeletable(Model $model)
    {
        return true;
    }
}
