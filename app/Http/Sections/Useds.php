<?php

namespace App\Http\Sections;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\Slider;
use App\Models\Used;
use App\Models\UsedCategory;
use App\Models\UsedPhoto;
use App\Services\FileServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;

/**
 * Class Useds
 *
 * @property Used $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Useds extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = "Товары";

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {}

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   */
  public function onDisplay($payload = [])
  {
    $display = AdminDisplay::datatablesAsync();
    $display->setHtmlAttribute('class', 'table-primary')
      ->setColumns([
        AdminColumn::link('id', '#')->setWidth('50px'),
        AdminColumn::link('category.name', 'Категория'),
        AdminColumn::link('name', 'Название')
      ])
      ->setOrder([1, 'ASC'])
      ->paginate(20);

    $display->setColumnFilters([
      null,
      AdminColumnFilter::select(new UsedCategory())
        ->setDisplay('name')
        ->setPlaceholder('Категория')
        ->setColumnName('category_id'),
    ])->setPlacement('table.header');

    return $display;
  }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   * @throws \Exception
   */
  public function onEdit($id = null, $payload = [])
  {
    $images = AdminFormElement::images('admin_image', 'Фото')
      ->setValidationRules(['admin_image' => 'image|mimes:png,gif,jpg,jpeg|max:2048'])
      ->setUploadFileName(function (\Illuminate\Http\UploadedFile $file) use ($id) {
        return Str::random(32) . '.' . $file->getClientOriginalExtension() ?? 'jpg';
      })
      ->setUploadPath(
        function (UploadedFile $file) use ($id) {
          $path = UsedPhoto::PHOTO_PATH . $id . '/';
          if (!File::exists(public_path($path), 0777, true, true)) {
            File::makeDirectory(public_path($path), 0777, true, true);
          }
          return $path;
        })
      ->setSaveCallback(function ($file, $path, $filename, $settings) use ($id) {
        $image = \Intervention\Image\Facades\Image::make($file);
        $image->resize(360, 470)->save(public_path() . $path . $filename);
//          foreach (Slider::SIZES as $index => $size) {
//            $size = explode('x', $size);
//            $name = $index . '_' . $filename;
//            $image->resize($size[0], $size[1])->save(public_path() . $path . $name);
//          }
        return ['path' => $path, 'value' => $path . $filename];
      });

    $form = AdminForm::panel()->addBody([
      AdminFormElement::select('category_id', 'Категория', UsedCategory::all()->pluck('name', 'id')->toArray()),
      AdminFormElement::text('name', 'Название')->required(),
      AdminFormElement::number('price', 'Цена')->required(),
      AdminFormElement::wysiwyg('description', 'Описание'),
      ($id) ? $images : AdminFormElement::html('<p>Загрузка фото доступна после сохранения!</p>', function (){}),
      AdminFormElement::hidden('slug'),
    ]);

    return $form;
  }

  /**
   * @param array $payload
   * @return FormInterface
   * @throws \Exception
   */
  public function onCreate($payload = [])
  {
    return $this->onEdit(null, $payload);
  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    return true;
  }
}
