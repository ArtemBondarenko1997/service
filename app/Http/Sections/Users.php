<?php

namespace App\Http\Sections;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;

/**
 * Class Users
 *
 * @property User $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Users extends Section implements Initializable
{
  /**
   * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
   *
   * @var bool
   */
  protected $checkAccess = false;

  /**
   * @var string
   */
  protected $title = "Пользователи";

  /**
   * @var string
   */
  protected $alias;

  /**
   * Initialize class.
   */
  public function initialize()
  {
    $this->addToNavigation()->setPriority(100)->setIcon('fa fa-users');
  }

  /**
   * @param array $payload
   *
   * @return DisplayInterface
   */
  public function onDisplay($payload = [])
  {
    $display = AdminDisplay::datatablesAsync()
      ->setHtmlAttribute('class', 'table-primary')
      ->setColumns(
        AdminColumn::link('id', '#')->setWidth('30px'),
        AdminColumn::link('name', 'Имя'),
        AdminColumn::text('email', 'Email'),
        AdminColumn::datetime('created_at', 'Дата создания')->setFormat('Y m d'),
        AdminColumn::datetime('updated_at', 'Дата изменения')->setFormat('Y m d')
      )
      ->setOrder([1, 'ASC'])
      ->paginate(20);

    return $display;
  }

  /**
   * @param int|null $id
   * @param array $payload
   *
   * @return FormInterface
   */
  public function onEdit($id = null, $payload = [])
  {
    $form = AdminForm::panel()->addBody([
      AdminFormElement::text('name', 'Имя')->required(),
      AdminFormElement::text('email', 'Email')
        ->setValidationRules(['email' => 'unique:users,email,' . $id])
        ->addValidationMessage('unique', 'Такой EMAIL уже существует')
        ->required(),
      AdminFormElement::text('secret', 'Пароль'),
//      AdminFormElement::text('address', 'Адресс'),
//      AdminFormElement::text('lat', 'Долгота')->setValidationRules(['lat' => 'nullable|numeric'])
//        ->addValidationMessage('numeric', 'Значение должно иметь формат - 11.000000'),
//      AdminFormElement::text('lng', 'Широта')->setValidationRules(['lng' => 'nullable|numeric'])
//        ->addValidationMessage('numeric', 'Значение должно иметь формат - 11.000000'),
    ]);

    return $form;
  }

  /**
   * @param array $payload
   * @return FormInterface
   */
  public function onCreate($payload = [])
  {
    $form = AdminForm::panel()->addBody([
      AdminFormElement::text('name', 'Имя')->required(),
      AdminFormElement::text('email', 'Email')
        ->setValidationRules(['email' => 'unique:users,email'])
        ->addValidationMessage('unique', 'Такой EMAIL уже существует')
        ->required(),
      AdminFormElement::text('secret', 'Пароль')->required(),
    ]);

    return $form;
  }

  /**
   * @param Model $model
   * @return bool
   */
  public function isDeletable(Model $model)
  {
    return true;
  }
}
