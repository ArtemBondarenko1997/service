<?php

namespace App\Http\Sections\Articles;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use App\Models\Article;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;

/**
 * Class Articles
 *
 * @property \App\Models\Article $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class ArticlesOther extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Другие брэнды';

    /**
     * @var string
     */
    protected $alias = 'article-other';

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fa fa-newspaper');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
      $display = AdminDisplay::datatablesAsync();
      if(request()->get('type', null)){
        $display->setApply(function ($query) {

          $query->where('page', '=', request()->get('type'));
        });
      }
      $display->setHtmlAttribute('class', 'table-primary')
        ->setColumns([
          AdminColumn::link('id', '#')->setWidth('30px'),
          AdminColumn::link('page_name', 'Страница'),
          AdminColumn::text('position_name', 'Порядок'),
          AdminColumn::custom('Публикация', function (Article $article) {
            return $article->publish ? '<i class="fa fa-check"></i>' : '<i class="fa fa-minus"></i>';
          }),
          AdminColumn::datetime('created_at', 'Дата создания')->setFormat('Y m d'),
          AdminColumn::datetime('updated_at', 'Дата изменения')->setFormat('Y m d')
        ])->paginate(20);

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
      $positions = Article::POSITIONS['other'];

      $form = AdminForm::panel()->addBody([
        AdminFormElement::text('page', '')->setDefaultValue('other')->replaceHtmlAttribute('style', 'display:none'),
        AdminFormElement::select('position', 'Порядок', $positions)->required(),
        AdminFormElement::wysiwyg('text', 'Статья'),
        AdminFormElement::checkbox('publish', 'Публикация'),
      ]);

      return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        return $this->onEdit(null, $payload);
    }

    /**
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
