<?php

namespace App\Http\Controllers\Api;

use App\Models\Contact;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
  /**
   * @return JsonResponse
   */
  public function all()
    {
      $contacts = Contact::where('is_publish', '=', true)->get()->groupBy('sys_type');

      return response()->json($contacts, 200);
    }
}
