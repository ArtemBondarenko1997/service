<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\GadgetServicePivot;
use App\Models\Service;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
  /**
   * @param Category $category
   * @return JsonResponse
   */
  public function modules(Category $category)
  {
    $modules = $category->modulePivot;

    return response()->json($modules, 200);
  }

  public function module(Category $category, $service_id)
  {
    $service = $service = Service::find($service_id);
    $id_gadgets = $category->gadgets->pluck('id')->toArray();
    $gadgets = GadgetServicePivot::with(['gadget'])
      ->select('gadget_service.*')
      ->join('gadgets', 'gadgets.id', '=', 'gadget_service.gadget_id')
      ->whereIn('gadget_id', $id_gadgets)
      ->where('service_id', '=', $service_id)
      ->orderBy('gadgets.order')
      ->get();

    return response()->json([
      'gadgets' => $gadgets,
      'service' => $service
    ], 200);
  }
}
