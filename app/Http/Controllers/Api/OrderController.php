<?php

namespace App\Http\Controllers\Api;

use App\Mail\MailOrder;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class OrderController extends Controller
{
    public function makeOrder(Request $request)
    {
      $request->validate(
        [
          'phone' => 'required|string',
          'name' => 'required|string|max:255',
          'device' => 'required|string',
          'description' => 'required|string',
        ]
      );

      $order = Order::create($request->only(Order::API_FIELDS));
      Mail::to(['gsmroomdnepr@gmail.com', 'penguin1274@gmail.com'])->send(new MailOrder($order));

      return response()->json($order, 200);
    }
}
