<?php

namespace App\Http\Controllers\Api;

use App\Models\Article;
use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    public function get()
    {
      return response()->json([
        'articles' => Article::where('publish', '=', true)->get(),
      ], 200);
    }

    public function getByPage($page)
    {
      return response()->json([
        'articles' => Article::where('publish', '=', true)->where('page', '=', $page)->get()->pluck('text', 'position'),
        'main_images' => $main_images = Image::mainImages()->get()->pluck('path', 'type'),
      ], 200);
    }
}
