<?php

namespace App\Http\Controllers\Api;

use App\Models\Used;
use App\Models\UsedCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsedController extends Controller
{
  /**
   * @param null $category
   * @return JsonResponse
   */
  public function useds($category = null)
  {
    $categories = UsedCategory::all();
    $query = Used::query()->orderBy('created_at', 'desc');
    if($category){
      $useds = $query->where('category_id', '=', $category)->get();
    }else{
      $useds = $query->get();
    }

    return response()->json([
      'useds' => $useds,
      'categoties' => $categories,
    ], 200);
  }
}
