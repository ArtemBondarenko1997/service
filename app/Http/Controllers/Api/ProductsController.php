<?php

namespace App\Http\Controllers\Api;

use App\Models\Used;
use App\Models\UsedCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
  /**
   * @param null $category
   * @return JsonResponse
   */
  public function products($category = null)
    {
      $categories = UsedCategory::all();
      $query = Used::query()->orderBy('created_at', 'desc');
      if($category){
        $products = $query->where('category_id', '=', $category)->get();
      }else{
        $products = $query->get();
      }

      return response()->json([
        'products' => $products,
        'categories' => $categories,
      ], 200);
    }

  /**
   * @param $slug
   * @return JsonResponse
   */
  public function product($slug)
    {
      $product = Used::where('slug', '=', $slug)->first();

      return response()->json($product, 200);
    }
}
