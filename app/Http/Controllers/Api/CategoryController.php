<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\Gadget;
use App\Models\Image;
use App\Models\Slider;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
  /**
   * @return JsonResponse
   */
  public function getCategories()
  {
    $categories = Category::orderBy('order', 'asc')->get();
    $sliders = Slider::orderBy('order')->get();
    $main_images = Image::mainImages()->get()->pluck('path', 'type');
    return response()->json([
      'categories' => $categories,
      'sliders' => $sliders,
      'main_images' => $main_images
    ], 200);
  }

  /**
   * @param $slug
   * @return JsonResponse
   */
  public function getGadgets($slug)
  {
    $category = Category::where('slug', '=', $slug)->first();
    $gadgets = $category->gadgets()->orderBy('order', 'asc')->get();
    $modules = $category->modulePivot;

    return response()->json([
      'gadgets' => $gadgets,
      'modules' => $modules,
    ], 200);
  }

  /**
   * @param $slug
   * @return JsonResponse
   */
  public function getGadget($slug)
  {
    $gadgetWithServices = Gadget::with('treeService')
      ->where('slug', '=',$slug)
      ->orderBy('order', 'asc')
      ->get();

    return response()->json([
      'gadget' => $gadgetWithServices
    ], 200);
  }
}
