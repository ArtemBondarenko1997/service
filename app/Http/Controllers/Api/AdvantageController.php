<?php

namespace App\Http\Controllers\Api;

use App\Models\Advantage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdvantageController extends Controller
{
  /**
   * @return JsonResponse
   */
  public function get()
    {
      $advantages = Advantage::where('publish', '=', true)
        ->orderBy('order', 'asc')
        ->get();

      return response()->json([
        'advantages' => $advantages,
      ], 200);
    }


  /**
   * @param Advantage $advantage
   * @return JsonResponse
   */
  public function single(Advantage $advantage)
    {
      return response()->json([
        'advantage' => $advantage,
      ], 200);
    }
}
