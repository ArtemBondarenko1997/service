<?php

namespace App\Http\Controllers\Api;

use App\Models\GadgetServicePivot;
use App\Models\Service;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SelectController extends Controller
{
    public function gadgetDependent(Request $request)
    {

      $pivot = GadgetServicePivot::where('id', '=', $request->get('depdrop_parents')[0])->value('service_id');
      $isset_service = GadgetServicePivot::where('parent_id', '=', $request->get('depdrop_parents')[0])->get()->pluck('service_id')->toArray();
//      $options = Service::select('id', 'name')->where('parent_id', '=', $pivot)->whereIn('id', $isset_service)->get();
      $options = Service::select('id', 'name')->where('parent_id', '=', $pivot)->get();

      return new JsonResponse([
        'output' => collect($options)->map(function ($value, $key) {
          return ['id' => $value->id, 'name' => $value->name];
        }),
        'selected' => null,
      ]);
    }
}
