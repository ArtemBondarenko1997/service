<?php

namespace App\Http\Controllers\Api;

use App\Models\Slider;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
  /**
   * @return JsonResponse
   */
  public function all()
    {
      $sliders = Slider::orderBy('order', 'asc')->get();

      return response()->json($sliders, 200);
    }
}
