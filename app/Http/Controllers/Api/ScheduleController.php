<?php

namespace App\Http\Controllers\Api;

use App\Models\Schedule;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{
  /**
   * @return JsonResponse
   */
  public function get()
    {

      $schedules = Schedule::orderBy('day', 'asc')->get();

      $schedules = $schedules->mapWithKeys(function ($item) {
        return [$item['day'] => $item];
      });


      return response()->json([
        'schedules' => $schedules,
      ], 200);
    }
}
