<?php

namespace App\Http\Controllers\Api;

use App\Models\Work;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WorkController extends Controller
{
  /**
   * @return JsonResponse
   */
  public function works()
    {
      $works = Work::all();
      return response()->json($works, 200);
    }
}
