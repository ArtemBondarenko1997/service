<?php

namespace App\Console\Commands;

use App\Models\GadgetServicePivot;
use Illuminate\Console\Command;

class FilterPivotService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'filter:pivot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete service where deleted parent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $parent_id = [];
      foreach(GadgetServicePivot::whereNotNull('parent_id')->get() as $service){

        $parent = GadgetServicePivot::find($service->parent_id);
        if(!$parent){
          $parent_id[$service->parent_id] = $service->parent_id;
        }
      }

      \DB::table('gadget_service')->whereIn('parent_id', $parent_id)->delete();
    }
}
