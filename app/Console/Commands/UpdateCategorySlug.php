<?php

namespace App\Console\Commands;

use App\Models\Category;
use Illuminate\Console\Command;
use Str;

class UpdateCategorySlug extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'category:slug';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Updated categories slug';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $this->info('Updated categories slug');

    $categories = Category::all();

    \DB::beginTransaction();

    foreach ($categories as $category) {

      \DB::table('categories')
        ->where('id', '=', $category->id)
        ->update([
          'slug' => Str::slug(strtolower($category->name), '-')
        ]);
    }

    \DB::commit();
  }
}
