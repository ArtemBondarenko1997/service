<?php

namespace App\Console\Commands;

use App\Models\Gadget;
use Illuminate\Console\Command;
use Str;

class UpdateGadgetSlug extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'gadget:slug';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Updated gadgets slug';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $this->info('Updated gadgets slug');

    $gadgets = Gadget::all();

    \DB::beginTransaction();

    foreach ($gadgets as $gadget) {

      \DB::table('gadgets')
        ->where('id', '=', $gadget->id)
        ->update([
          'slug' => Str::slug(strtolower($gadget->model), '-')
        ]);
    }

    \DB::commit();
  }
}
