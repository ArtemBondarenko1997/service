<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsedPhoto extends Model
{
  const PHOTO_PATH = '/images/useds/';

  protected $guarded = [];

  protected $appends = ['admin_image'];

  /*
   * Mutator
   */
  public function getAdminImageAttribute()
  {
    return '/images/useds/' . $this->attributes['used_id'] . '/' . $this->attributes['name'];
  }

  public function setAdminImageAttribute($value)
  {
    return str_replace('/images/useds/' . $this->attributes['used_id'] . '/', '', $value);
  }

  /*
   * Relation
   */

  public function used()
  {
    return $this->belongsTo(Used::class, 'used_id', 'id');
  }
}
