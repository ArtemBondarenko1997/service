<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class GadgetServicePivot extends Model
{
  protected $table = 'gadget_service';

  protected $appends = ['service_name'];

  protected static function boot()
  {
    parent::boot();
    static::saving(function ($model) {
      if (!$model->price) {
        $model->price = 0;
      }
    });

    static::deleted(function ($model) {
      $model->children()->delete();
    });
  }

  /*
   * Mutator
   */

  /**
   * @return mixed
   */
  public function getServiceNameAttribute()
  {
    return $this->service->name;
  }

  public function getIsFromAttribute()
  {
    return ($this->attributes['is_from']) ? true : false;
  }

  /*
   * Relation
   */

  /**
   * @return HasOne
   */
  public function service()
  {
    return $this->hasOne(Service::class, 'id', 'service_id');
  }

  /**
   * @return BelongsTo
   */
  public function gadget()
  {
    return $this->belongsTo(Gadget::class, 'gadget_id', 'id');
  }

  /**
   * @return HasMany
   */
  public function gadgets()
  {
    return $this->hasMany(Gadget::class, 'id', 'gadget_id')->orderBy('order');
  }

  /**
   * @return BelongsTo
   */
  public function parent()
  {
    return $this->belongsTo(GadgetServicePivot::class, 'id', 'parent_id');
  }

  /**
   * @return HasMany
   */
  public function children()
  {
    return $this->hasMany(GadgetServicePivot::class, 'parent_id', 'id')
      ->orderBy('order', 'asc');
  }

  public function parentService()
  {
    return $this->hasOne(Service::class, 'id', 'parent_id');
  }


  /**
   * @return HasMany
   */
  public function childrenService()
  {
    return $this->hasMany(Service::class, 'parent_id', 'service_id');
  }
}
