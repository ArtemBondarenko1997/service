<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

/**
 * @method static where(string $string, string $string1, $slug)
 * @method static orderBy(string $string, string $string1)
 */
class Used extends Model
{
  protected $guarded = [];

  protected $appends = ['admin_image'];

  /*
   * Mutator
   */
  public function getAdminImageAttribute()
  {
      return $this->photos->pluck('admin_image')->toArray();
  }

  public function setAdminImageAttribute()
  {
    return '';
  }

  public function setSlugAttribute($value = null)
  {
    return $this->attributes['slug'] = Str::slug($this->attributes['name'], '-', 'en');
  }

  /*
   * Relation
   */

  /**
   * @return HasMany
   */
  public function photos()
  {
    return $this->hasMany(UsedPhoto::class, 'used_id', 'id')->orderBy('is_main', 'desc');
  }

  /**
   * @return BelongsTo
   */
  public function category()
  {
    return $this->belongsTo(UsedCategory::class, 'category_id', 'id');
  }

}
