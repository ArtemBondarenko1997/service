<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

//  USER DEFAULT
//  name = Admin
//  email = admin@admin.com
//  password = admin

/**
 * @method static create(array $array)
 */
class User extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = [];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  /**
   * Mutator
   */
  public function setSecretAttribute($value)
  {
    if ($value) {
      return $this->attributes['password'] = \Hash::make($value);
    }
  }

  public function getSecretAttribute()
  {
    return '';
  }
}
