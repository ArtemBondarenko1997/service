<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Advantage
 * @package App\Models
 *
 * @property string $title
 * @property string $description
 * @property string $image
 * @property boolean $publish
 * @property integer $order
 */
class Advantage extends Model
{

  const FOLDER = 'advantages';

  protected $fillable = ['title', 'description', 'image', 'publish', 'order'];

  protected $dates = [
    'created_at', 'updated_at'
  ];
}
