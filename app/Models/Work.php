<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($id)
 */
class Work extends Model
{
  const FOLDER = 'works';

  protected $guarded = [];

  protected $appends = ['route', 'category_slug', 'device_name'];

  /*
   * Mutator
   */
  public function getRouteAttribute()
  {
    if ($this->attributes['gadget_slug']) {
      return route('gadget', ['slug' => $this->attributes['gadget_slug']]);
    }
    return $this->attributes['gadget_slug'];
  }

  public function getCategorySlugAttribute()
  {
      if($this->attributes['gadget_slug'] && $this->gadget){
        return $this->gadget->category->slug;
      }

      return null;
  }

  public function getDeviceNameAttribute()
  {
    if($this->attributes['gadget_slug'] && $this->gadget){
      return $this->gadget->model;
    }
    return null;
  }

  /*
   * Relation
   */
  public function gadget()
  {
    return $this->hasOne(Gadget::class, 'slug', 'gadget_slug');
  }


}
