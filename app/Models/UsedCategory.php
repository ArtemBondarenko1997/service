<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class UsedCategory extends Model
{
    protected $guarded = [];

    /*
     * Mutator
     */
    public function setSlugAttribute($value = null)
    {
      return  $this->attributes['slug'] = Str::slug($this->attributes['name'], '-', 'en');
    }

    /*
     * Relation
     */
  /**
   * @return HasMany
   */
  public function useds()
    {
      return $this->hasMany(Used::class, 'category_id', 'id');
    }
}
