<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Schedule
 * @package App\Models
 *
 * @property integer $day
 * @property string $work
 */
class Schedule extends Model
{
  protected $fillable = ['day', 'work'];

  protected $dates = [
    'created_at', 'updated_at'
  ];

  protected $appends = ['day_name', 'short_day'];

  const DAYS = [
    1 => 'Понедельник',
    2 => 'Вторник',
    3 => 'Среда',
    4 => 'Четверг',
    5 => 'Пятница',
    6 => 'Суббота',
    7 => 'Воскресенье',
  ];

  const SHORT_DAYS = [
    1 => 'Пн',
    2 => 'Вт',
    3 => 'Ср',
    4 => 'Чт',
    5 => 'Пт',
    6 => 'Сб',
    7 => 'Вс',
  ];

  /*
   * Mutator
   */
  public function getDayNameAttribute()
  {
    return self::DAYS[$this->attributes['day']] ?? $this->attributes['day'];
  }

  public function getShortDayAttribute()
  {
    return self::SHORT_DAYS[$this->attributes['day']] ?? $this->attributes['day'];
  }

  public function setDayNameAttribute($value)
  {
    return null;
  }

  public function setShortDayAttribute()
  {
    return null;
  }
}
