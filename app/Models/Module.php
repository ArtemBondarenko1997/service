<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static find($id)
 * @method static whereNotIn(string $string, $issetId)
 */
class Module extends Model
{
    const FOLDER = 'modules';

    protected $guarded = [];

    /*
     * Relation
     */

  /**
   * @return BelongsTo
   */
  public function category()
    {
      return $this->belongsTo(Category::class, 'category_id', 'id');
    }

  /**
   * @return BelongsToMany
   */
  public function service()
    {
      return $this->belongsToMany(Service::class, 'category_module', 'module_id', 'service_id');
    }

  /**
   * @return HasMany
   */
  public function pivot()
    {
      return $this->hasMany(CategoryModule::class, 'module_id', 'id');
    }
}
