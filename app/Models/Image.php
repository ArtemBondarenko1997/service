<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App\Models
 *
 * @property string $type
 * @property string $path
 */
class Image extends Model
{
  const FOLDER = 'main';
  const MAIN_IMAGES = ['logo', 'main_head_left', 'main_head_right'];
  const TYPES = [
    'logo' => 'Лого',
    'main_head_left' => 'На главной в шапке левая',
    'main_head_right' => 'На главной в шапке правая',
  ];

  const LOGO_RESOLUTION = [
    'width' => 150,
    'height' => 150,
  ];

  const RESOLUTION = [
    'width' => 342,
    'height' => 120,
  ];

  protected $fillable = ['type', 'path'];

  protected $dates = [
    'created_at',
    'updated_at'
  ];


  /*
   * Mutator
   */
  public function getNameAttribute()
  {
    if(isset(self::TYPES[$this->type])){
      return self::TYPES[$this->type];
    }
    return $this->attributes['type'];
  }

  public function setNameAttribute($value)
  {
    return null;
  }

  /*
   * Scope
   */
  public function scopeMainImages($query){
    return $query->whereIn('type', self::MAIN_IMAGES);
  }


  /**
   * @return array
   */
  public function getResolution()
  {
    if($this->type == 'logo'){
      return self::LOGO_RESOLUTION;
    }

    return self::RESOLUTION;

  }
}
