<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class CategoryModule extends Model
{
  protected $table = 'category_module';

  protected $guarded = [];

  protected $appends = ['module_name', 'service_name'];

  /*
   * Mutator
   */
  public function getModuleNameAttribute()
  {
    return $this->module->name;
  }

  public function getServiceNameAttribute()
  {
    return $this->service->name;
  }

  /*
   * Relation
   */

  /**
   * @return BelongsTo
   */
  public function service()
  {
    return $this->belongsTo(Service::class, 'service_id', 'id');
  }

  /**
   * @return HasOne
   */
  public function module()
  {
    return $this->hasOne(Module::class, 'id', 'module_id');
  }

  public function category()
  {
    return $this->belongsTo(Category::class, 'category_id', 'id');
  }
}
