<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Str;

/**
 * @method static create(array $arr)
 * @method static insert(array $arr)
 * @method static find($id)
 * @method static orderBy(string $string, string $string1)
 * @method static where(string $string, string $string1, $slag)
 */
class Category extends Model
{

  const FOLDER = 'categories';

  protected $guarded = [];

  protected $dates = [
    'created_at',
    'updated_at',
  ];

  protected $appends = ['logo_src', 'icon_src'];

  /*
   * Mutator
   */

  public function setSlugAttribute($value)
  {
    $this->attributes['slug'] = Str::slug(strtolower($this->name), '-');
  }

  public function getLogoSrcAttribute()
  {
    if ($this->attributes['logo']) {
      return '/' . $this->attributes['logo'];
    }
    return $this->attributes['logo'];
  }

  public function getIconSrcAttribute()
  {
    if ($this->attributes['icon']) {
      return '/' . $this->attributes['icon'];
    }
    return $this->attributes['icon'];
  }

  /*
   * Relation
   */

  /**
   * @return HasMany
   */
  public function gadgets()
  {
    return $this->hasMany(Gadget::class, 'category_id', 'id');
  }

  /**
   * @return HasMany
   */
  public function modulePivot()
  {
    return $this->hasMany(CategoryModule::class, 'category_id', 'id');
  }
}
