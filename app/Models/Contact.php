<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
  protected $guarded = [];

  protected $appends = ['user_contact'];

  public function getUserContactAttribute()
  {
    return [];
  }
}
