<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Str;

/**
 * Class Gadget
 * @package App\Models
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $model
 * @property string $title
 * @property string $description
 * @property string $logo
 * @property string $icon
 * @property string $slug
 * @property integer $order
 * @property boolean $is_publish
 * @property string $article
 */
class Gadget extends Model
{

  const FOLDER = 'gadgets';

  protected $guarded = [];

  protected $dates = [
    'created_at',
    'updated_at',
  ];

  protected $appends = ['logo_src', 'icon_src'];

  /*
 * Mutator
 */

  public function setSlugAttribute($value)
  {
    $this->attributes['slug'] = Str::slug(strtolower($this->model), '-');
  }

  public function getLogoSrcAttribute()
  {
    if ($this->attributes['logo']) {
      return '/' . $this->attributes['logo'];
    }
    return $this->attributes['logo'];
  }

  public function getIconSrcAttribute()
  {
    if ($this->attributes['icon']) {
      return '/' . $this->attributes['icon'];
    }
    return $this->attributes['icon'];
  }

  /*
   * Relation
   */
  /**
   * @return BelongsTo
   */
  public function category()
  {
    return $this->belongsTo(Category::class, 'category_id', 'id');
  }

  /**
   * @return BelongsToMany
   */
  public function services()
  {
    return $this->belongsToMany(Service::class, 'gadget_service', 'gadget_id', 'service_id')->withPivot('price', 'time', 'is_from');
  }

  /**
   * @return HasMany
   */
  public function pivot()
  {
    return $this->hasMany(GadgetServicePivot::class, 'gadget_id', 'id');
  }

  /**
   * @return HasMany
   */
  public function treeService()
  {
    return $this->hasMany(GadgetServicePivot::class, 'gadget_id', 'id')
      ->whereNull('parent_id')->orderBy('order', 'asc')
      ->with('children');
  }

  /**
   * @return HasMany
   */
  public function pivotParent()
  {
    return $this->hasMany(GadgetServicePivot::class, 'gadget_id', 'id')
      ->whereNull('parent_id');
  }

  /**
   * @return HasMany|Builder
   */
  public function pivotChildren()
  {
    return $this->hasMany(GadgetServicePivot::class, 'gadget_id', 'id')
      ->whereNotNull('parent_id');
  }
}
