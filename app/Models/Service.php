<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static insert(array $array)
 * @method static whereNotNull(string $string)
 * @method static whereNull(string $string)
 */
class Service extends Model
{
    protected $guarded = [];

    protected $dates = [
      'created_at',
      'updated_at',
    ];

    protected $appends = ['service_id'];

    public function serviceModulePivot()
    {
      return $this->hasMany(CategoryModule::class, 'service_id', 'id');
    }

    public function serviceGadgetPivot()
    {
      return $this->hasMany(GadgetServicePivot::class, 'service_id', 'id')->with(['gadgets']);
    }

  public function gadgets()
  {
    return $this->belongsToMany(Gadget::class, 'gadget_service', 'service_id', 'gadget_id');
  }

  /**
   * @return HasMany
   */
  public function children()
  {
    return $this->hasMany(Service::class, 'parent_id', 'id');
  }

  public function getServiceIdAttribute()
  {
    return $this->id;
  }
}
