<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 * @package App\Models
 *
 * @property string $page
 * @property string $position
 * @property string $text
 * @property boolean $publish
 */
class Article extends Model
{
  protected $fillable = [
    'page', 'position', 'text', 'publish'
  ];

  protected $dates = [
    'created_at', 'updated_at'
  ];

  protected $appends = ['page_name', 'position_name', 'position_admin'];

  const PAGES = [
    'main' => 'Главная',
    'other' => 'Другие бренды'
  ];

  const POSITIONS = [
    'main' => ['first' => 'Первая', 'second' => 'Вторая'],
    'other' => ['first' => 'Первая']
  ];

  /*
   * Mutator
   */
  public function getPageNameAttribute()
  {
    return self::PAGES[$this->attributes['page']] ?? $this->attributes['page'];
  }

  public function getPositionNameAttribute()
  {

    $position = $this->attributes['position'];
    return self::POSITIONS[$this->attributes['page']][$position] ?? $position;
  }

  public function getPositionAdminAttribute()
  {
    $position = $this->attributes['position'];
    return self::POSITIONS[$this->attributes['page']];
  }

}
