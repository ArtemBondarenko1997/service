<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Traits\OrderableModel;

/**
 * @method static find($id)
 * @method static orderBy(string $string)
 */
class Slider extends Model
{

  use OrderableModel;

  const FOLDER = 'sliders';

  protected $guarded = [];
}
