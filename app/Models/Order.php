<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App\Models
 *
 * @property integer $id
 * @property string $phone
 * @property string $name
 * @property string $description
 * @property boolean $is_read
 * @property boolean $is_courier
 * @property boolean $device
 * @property string $order_title
 */
class Order extends Model
{
    protected $fillable = [
      'phone',
      'name',
      'description',
      'is_read',
      'device',
      'is_courier'
    ];

    protected $dates = [
      'created_at',
      'updated_at',
    ];

    protected $appends = ['order_title'];

    const API_FIELDS = ['name', 'phone', 'device', 'description', 'is_courier'];

    /*
     * Mutator
     */

    public function getOrderTitleAttribute()
    {
      $is_courier = ($this->is_courier) ? ' (Доставка с курьером)' : '';

      return 'Новая заявка  ' . $is_courier . ': ' . $this->id;
    }
}
