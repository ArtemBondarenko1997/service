<?php

namespace App\Observers;

use App\Models\Work;
use App\Services\FileServices;

class WorkObserver
{
  protected $fileService;

  public function __construct(FileServices $fileService)
  {
    $this->fileService = $fileService;
  }
  /**
   * Handle the slider "updated" event.
   *
   * @param  Work  $work
   * @return void
   */
  public function updating(Work  $work)
  {
    $oldWork = Work::find($work->id);
    $this->fileService->deleteBeforeSave($work, 'after', $oldWork);
    $this->fileService->deleteBeforeSave($work, 'before', $oldWork);
  }

  /**
   * Handle the slider "deleted" event.
   *
   * @param  Work  $work
   * @return void
   */
  public function deleted(Work  $work)
  {
    $this->fileService->deleteFile($work, 'after');
    $this->fileService->deleteFile($work, 'before');
  }

  /**
   * @param Work  $work
   */
  public function saving(Work  $work)
  {
    $oldWork = Work::find($work->id);
    $this->fileService->deleteBeforeSave($work, 'after', $oldWork);
    $this->fileService->deleteBeforeSave($work, 'before', $oldWork);
  }
}
