<?php

namespace App\Observers;

use App\Models\Service;

class ServiceObserver
{
  /**
   * @param Service $service
   */
  public function deleting(Service $service)
  {
    $service->serviceModulePivot()->delete();
    $service->serviceGadgetPivot()->delete();

  }

  /**
   * Handle the service "deleted" event.
   *
   * @param Service $service
   * @return void
   */
  public function deleted(Service $service)
  {
    $service->children()->delete();
  }
}
