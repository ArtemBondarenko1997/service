<?php

namespace App\Observers;

use App\Models\Module;
use App\Services\FileServices;

class ModuleObserver
{
  protected $fileService;

  public function __construct(FileServices $fileService)
  {
    $this->fileService = $fileService;
  }
  /**
   * Handle the slider "updated" event.
   *
   * @param  Module $module
   * @return void
   */
  public function updating(Module $module)
  {
    $oldModule = Module::find($module->id);
    $this->fileService->deleteBeforeSave($module, 'icon', $oldModule);
  }

  /**
   * Handle the slider "deleted" event.
   *
   * @param  Module $module
   * @return void
   */
  public function deleted(Module $module)
  {
    $this->fileService->deleteFile($module, 'icon');
    $module->pivot()->delete();
  }

  /**
   * @param Module $module
   */
  public function saving(Module $module)
  {
    $oldModule = Module::find($module->id);
    $this->fileService->deleteBeforeSave($module, 'icon', $oldModule);
  }
}
