<?php

namespace App\Observers;

use App\Models\Gadget;
use App\Services\FileServices;

class GadgetObserver
{

  protected $fileService;

  public function __construct(FileServices $fileService)
  {
    $this->fileService = $fileService;
  }

  /**
   * @param Gadget $gadget
   */
  public function deleted(Gadget $gadget)
  {
    $this->fileService->deleteFile($gadget, 'icon');
    $this->fileService->deleteFile($gadget, 'logo');
    $gadget->pivot()->delete();
  }

  /**
   * @param Gadget $gadget
   */
  public function updating(Gadget $gadget)
  {
    $oldGadget = Gadget::find($gadget->id);
    $this->fileService->deleteBeforeSave($gadget, 'icon', $oldGadget);
    $this->fileService->deleteBeforeSave($gadget, 'logo', $oldGadget);
  }

  /**
   * @param Gadget $gadget
   */
  public function saving(Gadget $gadget)
  {
    $oldGadget = Gadget::find($gadget->id);
    $this->fileService->deleteBeforeSave($gadget, 'icon', $oldGadget);
    $this->fileService->deleteBeforeSave($gadget, 'logo', $oldGadget);
  }

}
