<?php

namespace App\Observers;

use App\Models\Used;
use App\Models\UsedPhoto;

class UsedObserver
{
  /**
   * Handle the used "created" event.
   *
   * @param Used $used
   * @return void
   */
  public function saving(Used $used)
  {
    $images = request()->get('admin_image', []);
    $this->updateImage($used, $images);
    $this->setMainImage($used, request()->get('main_photo', null));
  }

  /**
   * Handle the used "deleted" event.
   *
   * @param Used $used
   * @return void
   */
  public function deleting(Used $used)
  {
    $this->deleteImage($used, $used->photos->pluck('admin_image')->toArray());
  }

  public function updateImage(Used $used, $images = [])
  {
    $to_delete = array_diff($used->admin_image, $images);
    $to_save = array_diff($images, $used->admin_image);

    $this->deleteImage($used, $to_delete);
    $this->saveImages($used, $to_save);

  }

  public function saveImages(Used $used, $images = [])
  {
    $path = UsedPhoto::PHOTO_PATH . $used->id . '/';
    $data = [];
    if (!empty($images)) {
      foreach ($images as $image) {
        $data[] = [
          'used_id' => $used->id,
          'name' => str_replace($path, '', $image),
        ];
      }

      $used->photos()->insert($data);
    }
  }

  public function deleteImage(Used $used, $images = [])
  {
    $path = UsedPhoto::PHOTO_PATH . $used->id . '/';
    $data = [];

    if (!empty($images)) {
      foreach ($images as $image) {
        unlink(public_path($image));
        $data[] = str_replace($path, '', $image);
      }
      $used->photos()->whereIn('name', $data)->delete();
    }

    if (is_dir(public_path($path)) && empty(array_diff(scandir(public_path($path)), array('.', '..')))) {
      rmdir(public_path($path));
    }

  }

  public function setMainImage(Used $used, $main = null)
  {
    if($main !== null){
      $name = str_replace(UsedPhoto::PHOTO_PATH . $used->id . '/', '', $main);
      $used->photos()->update(['is_main' => false]);
      $used->photos()->where('name', '=', $name)->update(['is_main' => true]);
    }
  }
}
