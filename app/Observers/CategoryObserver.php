<?php

namespace App\Observers;

use App\Models\Category;
use App\Services\FileServices;

class CategoryObserver
{
  protected $fileService;

  public function __construct(FileServices $fileService)
  {
    $this->fileService = $fileService;
  }
  /**
   * Handle the category "deleted" event.
   *
   * @param Category $category
   * @return void
   */
  public function deleted(Category $category)
  {
    $this->fileService->deleteFile($category, 'icon');
    $this->fileService->deleteFile($category, 'logo');

    $category->modulePivot()->delete();
  }

  /**
   * @param Category $category
   */
  public function updating(Category $category)
  {
    $oldCategory = Category::find($category->id);
    $this->fileService->deleteBeforeSave($category, 'icon', $oldCategory);
    $this->fileService->deleteBeforeSave($category, 'logo', $oldCategory);
  }

  /**
   * @param Category $category
   */
  public function saving(Category $category)
  {
    $oldCategory = Category::find($category->id);
    $this->fileService->deleteBeforeSave($category, 'icon', $oldCategory);
    $this->fileService->deleteBeforeSave($category, 'logo', $oldCategory);
  }

}
