<?php

namespace App\Observers;

use App\Models\Slider;
use App\Services\FileServices;

class SliderObserver
{
  protected $fileService;

  public function __construct(FileServices $fileService)
  {
    $this->fileService = $fileService;
  }
    /**
     * Handle the slider "updated" event.
     *
     * @param  Slider  $slider
     * @return void
     */
    public function updating(Slider $slider)
    {
      $oldSlider = Slider::find($slider->id);
      $this->fileService->deleteBeforeSave($slider, 'path', $oldSlider);
    }

    /**
     * Handle the slider "deleted" event.
     *
     * @param  Slider  $slider
     * @return void
     */
    public function deleted(Slider $slider)
    {
      $this->fileService->deleteFile($slider, 'path');
    }

  /**
   * @param Slider $slider
   */
  public function saving(Slider $slider)
  {
    $oldSlider = Slider::find($slider->id);
    $this->fileService->deleteBeforeSave($slider, 'path', $oldSlider);
  }
}
