<?php

use SleepingOwl\Admin\Navigation\Page;

$customNavGadget = (Schema::hasTable('categories')) ? (new \App\Services\AdminNav())->gadgets() : null;
$customNavContact = (Schema::hasTable('contacts')) ? (new \App\Services\AdminNav())->contacts() : null;

return [
  [
    'icon' => 'fa fa-book',
    'title' => 'Справочники',
    'priority' => 900,
    'pages' => [
      (new Page(\App\Models\Service::class))->setIcon('fa fa-tasks'),
      (new Page(\App\Models\Module::class))->setIcon('fa fa-tasks'),
    ]
  ],
  [
    'icon' => 'fa fa-book',
    'title' => 'Б/У',
    'priority' => 900,
    'pages' => [
      (new Page(\App\Models\UsedCategory::class))->setIcon('fa fa-shopping-cart'),
      (new Page(\App\Models\Used::class))->setIcon('fa fa-tag'),
    ]
  ],
//  [
//    'icon' => 'fa fa-list',
//    'title' => 'Общие таблицы',
//    'priority' => 1000,
//    'pages' => [
//      (new Page(\App\Models\CategoryModule::class))->setIcon('fa fa-tasks'),
//      (new Page(\App\Models\GadgetServicePivot::class))->setIcon('fa fa-tasks'),
//      (new Page(\App\Models\Gadget::class))->setIcon('fa fa-tasks'),
//      (new Page(\App\Models\Contact::class))->setIcon('fa fa-tasks'),
//    ]
//  ],

  $customNavGadget,
  $customNavContact,

  [
    'icon' => 'fa fa-newspaper',
    'title' => 'Статьи',
    'priority' => 900,
    'pages' => [
      [
        'title' => 'Главная',
        'icon' => 'fa fa-newspaper',
        'url' => 'admin/article-main?type=main',
      ],
      [
        'title' => 'Другие брэнды',
        'icon' => 'fa fa-newspaper',
        'url' => 'admin/article-other?type=other',
      ],

    ]
  ],

  [
    'icon' => 'fa fa-sign-out-alt',
    'title' => 'Выйти',
    'priority' => 900,
    'url' => '/logout',
  ]
];
