<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class FileServices
{
  /**
   * @param Model $model
   * @param string $column
   */
  public function deleteFile(Model $model, $column = '')
  {
    if (is_file(realpath($model->{$column})) === true) {
      unlink(realpath($model->{$column}));
    }
  }

  /**
   * @param $model
   * @param $column
   * @param $oldModel
   */
  public function deleteBeforeSave($model, $column, $oldModel = null)
  {
    if ($oldModel) {
      $old_img = $oldModel->{$column};
      if ($old_img !== $model->{$column} && is_file(realpath($old_img)) === true) {
        unlink(realpath($old_img));
      }
    }
  }

  /**
   * @param $file
   * @param $name
   * @param $folder
   * @return string
   */
  public static function generateFileName($file, $name, $folder)
  {
    $pathFile = 'images/' . $folder . '/' . $name;
    if (File::exists($pathFile)) {
      $name = Str::random(32) . '.' . $file->getClientOriginalExtension() ?: '.png';
      FileServices::generateFileName($file, $name, $folder);
    }
    return $name;
  }
}
