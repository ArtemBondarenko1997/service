<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Contact;
use SleepingOwl\Admin\Navigation\Page;

class AdminNav
{
  /**
   * @return array
   */
  public function gadgets()
  {
    $categories = Category::all();
    $arr = [
      'icon' => 'fa fa-list',
      'title' => 'Девайсы',
      'priority' => 100,
      'pages' => [
//        (new Page(\App\Models\GadgetServicePivot::class))->setPriority(10)->setIcon('fa fa-tasks'),
      ],
    ];

    foreach ($categories as $category) {
      $arr['pages'][] = [
        'title' => $category->name,
        'icon' => 'fa fa-tablet',
        'url' => 'admin/gadget-' . $category->slug . '?category='.$category->id,
      ];
    }

    array_unshift($arr['pages'],  [ 'title' => 'Все девайсы', 'url' => 'admin/gadgets', 'icon' => 'fa fa-tasks']);
    return $arr;
  }

  public function contacts()
  {
    $names = [
      'phone' => [
        'name' => 'Телефоны',
        'icon' => 'fa fa-phone',
      ],
      'social' => [
        'name'=> 'Соц. сети',
        'icon'=> 'fa fa-comment'
      ],
      'address' => [
        'name'=> 'Адрес',
        'icon'=> 'fa fa-map-marker'
      ],
    ];
    $contacts = Contact::select('sys_type')->distinct('sys_type')->get()->pluck('sys_type');

    $arr = [
      'icon' => 'fa fa-address-book',
      'title' => 'Контакты',
      'priority' => 100,
      'pages' => [],
    ];

    foreach ($contacts as $contact) {
      if(isset($names[$contact])){
        $arr['pages'][] = [
          'title' => $names[$contact]['name'],
          'icon' => $names[$contact]['icon'],
          'url' => 'admin/contacts-' . $contact . '?slug='.$contact,
        ];
      }
    }

    return $arr;
  }
}
