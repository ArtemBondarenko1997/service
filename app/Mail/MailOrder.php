<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailOrder extends Mailable
{
  use Queueable, SerializesModels;

  protected $order;

  /**
   * Create a new message instance.
   *
   * @param Order $order
   */
  public function __construct(Order $order)
  {
    $this->order = $order;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    $subject = $this->order->order_title;

    return $this->view('vendor.mail.html.panel', [
      'order' => $this->order
    ])->subject($subject);

  }
}
