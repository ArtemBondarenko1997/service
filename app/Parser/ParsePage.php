<?php


namespace App\Parser;


use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\ChildNotFoundException;
use PHPHtmlParser\Exceptions\CircularException;
use PHPHtmlParser\Exceptions\CurlException;
use PHPHtmlParser\Exceptions\NotLoadedException;
use PHPHtmlParser\Exceptions\StrictException;

class ParsePage
{

  private $allUniqueService = [];

  /**
   * @param array $links
   * @return array
   * @throws ChildNotFoundException
   * @throws CircularException
   * @throws CurlException
   * @throws NotLoadedException
   * @throws StrictException
   */
  public function getGadgets()
  {
    $links = $this->getLinks();
    $links[] = "https://gsmroom.com.ua/remont-apple/remont-mac-mini/";
    $result = [];
    for ($i = 6; $i < count($links); $i++) {
      $dom = new Dom;
      $dom->loadFromUrl($links[$i]);
      $title = $dom->find('.entry-title')->text;
      $category_name = explode(' ', $title)[1];
      $description = $dom->find('.fusion-text')->innerHtml;
      $logo = $dom->find('.img-responsive')->tag->getAttribute('src')['value'];

      $result[$category_name][] = [
        'description' => $description,
        'title' => $title,
        'logo' => $logo,
        'services' => $this->parseServiceGadget($dom->find('table tr'))
      ];
    }
    $result['all_unique_services'] = $this->allUniqueService;
//    file_put_contents('gadgets.php', var_export($result, true));
    return $result;
  }

  /**
   * @throws ChildNotFoundException
   * @throws CircularException
   * @throws CurlException
   * @throws NotLoadedException
   * @throws StrictException
   */
  private function getLinks()
  {
    $dom = new Dom;
    $dom->loadFromUrl('https://gsmroom.com.ua/remont-apple/');

    $elements = $dom->find('.page_item a');
    $arr_links = [];
    foreach ($elements as $element) {
      if ($element->tag->getAttribute('href')['value'] === 'https://gsmroom.com.ua/remont-apple/')
        continue;
      if (in_array($element->tag->getAttribute('href')['value'], $arr_links))
        continue;
      $arr_links[] = $element->tag->getAttribute('href')['value'];
    }

    return $arr_links;
  }

  /**
   * @param $table
   * @return array
   */
  private function parseServiceGadget($table)
  {
    $result = [];
    foreach ($table as $raw) {
      $service = ($raw->find('.service')->count()) ? $raw->find('.service')->text : null;
      $price = ($raw->find('.price')->count()) ? $raw->find('.price')->text : null;
      $time = ($raw->find('.time')->count()) ? $raw->find('.time')->text : null;
      if (!$service && !$price && !$time) {
      } else {
        $result[] = [
          'service' => $service,
          'price' => $price,
          'time' => $time,
        ];
      }
      if ($service && !in_array($service, $this->allUniqueService)) {
        $this->allUniqueService[] = $service;
      }
    }
    return $result;
  }

}
