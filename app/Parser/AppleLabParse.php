<?php


namespace App\Parser;


use PHPHtmlParser\Dom;

class AppleLabParse
{
  const URL = 'https://apple-lab.com.ua/';

  public $categories;
  public $gadgets;
  public $services;

  public function parseCategory()
  {
    $dom = new Dom;
    $dom->loadFromUrl(self::URL);

    $elements = $dom->find('.products ul')[0]->find('li');

    $categories = [];
    foreach ($elements as $element) {

      $name = $element->find('.name')[0]->text;
      $href = $element->find('a')[0]->tag->getAttribute('href')['value'];
      $icon = 'img/products/' . str_replace('/', '', $href) . '.png';
      $iconBig = (str_replace('/', '', $href) != 'apple-tv') ? 'img/products/' . str_replace('/', '', $href) . '-big.png' : "";

      if (str_replace('/', '', $href) == 'apple-tv') {
        file_put_contents(public_path() . '/parseImage/' . str_replace('/', '', $href) . '-logo.png', file_get_contents(self::URL . 'images/pictures/new/' . str_replace('/', '', $href) . '-big.png'));
      } else {
        file_put_contents(public_path() . '/parseImage/' . str_replace('/', '', $href) . '-logo.png', file_get_contents(self::URL . 'img/heroes/' . str_replace('/', '', $href) . '.png'));
      }

      file_put_contents(public_path() . '/parseImage/' . str_replace('/', '', $href) . '.png', file_get_contents(self::URL . $icon));

      if (str_replace('/', '', $href) != 'apple-tv') {
        file_put_contents(public_path() . '/parseImage/' . str_replace('/', '', $href) . '-big.png', file_get_contents(self::URL . $iconBig));
      }
      $categories[$name] = [
        'href' => $href,
        'name' => $name,
        'title' => 'Ремонт ' . $name,
        'description' => str_repeat('<p>DESCRIPTION... </p><br>', 5),
        'slug' => str_replace('/', '', $href),
        'icon' => '/parseImage/' . str_replace('/', '', $href) . '.png',
        'iconBig' => '/parseImage/' . str_replace('/', '', $href) . '-big.png',
        'logo' => '/parseImage/' . str_replace('/', '', $href) . '-logo.png',
      ];

    }

    $this->categories = $categories;

    $this->parseGadget($categories);
    return $categories;

  }

  public function parseGadget($categories)
  {
    $gadgets = [];
    foreach ($categories as $key => $category) {
      $dom = new Dom();
      $elements = $dom->loadFromUrl(self::URL . $category['href']);

      if ($elements->find('.image')->count()) {
        $categories[$key]['gadgets'] = [];
        foreach ($elements->find('.image') as $element) {
          if (!$element->find('a')->count()) {
            continue;
          }
          $href = $element->find('a')->tag->getAttribute('href')['value'];

          $deviceDom = new Dom();
          $deviceDom->loadFromUrl(rtrim(self::URL, "/") . $href);

          $title = $deviceDom->find('.top')->text;
          $model = str_replace(['Ремонт', 'настройка', 'и'], '', $title);

          $this->getDescriptionImg($deviceDom->innerHtml);

          // TABLE
          if ($elements->find('table')->count()) {
            $services = $this->parceTable($elements->find('table')->innerHtml);
            $categories[$key]['gadgets'][$category['name']]['services'] = $services;
          }

          $icon = $element->find('img')->tag->getAttribute('src')['value'];

          $name = explode("/", $icon);
          file_put_contents(public_path() . '/parseImage/gadgets/' .end($name) , file_get_contents(rtrim(self::URL, "/") . $icon));
          $categories[$key]['gadgets'][$category['name']] = [
            'icon' => '/parseImage/gadgets/' .end($name),
            'title' => $title,
            'model' => trim($model),
          ];
        }

      } else {
        $categories[$key]['gadgets'][$category['name']] = [
          'icon' => $category['icon'],
          'logo' => $category['logo'],
          'model' => $category['name'],
        ];
        if ($elements->find('table')->count()) {
          $services = $this->parceTable($elements->find('table')->innerHtml);
          $categories[$key]['gadgets'][$category['name']]['services'] = $services;
        }
      }
    }

//    dd($categories);
  }

  public function parceTable($tableHTML)
  {
//    dd($tableHTML);
//    $dom = new Dom();
//    $dom->load($tableHTML);
//    $table = $dom->find('tbody tr');
//    $service_name = 'Основные';
//    $services = [];
//    foreach ($table as $row) {
//
//      if ($row->find('th')->count()) {
//        $service_name = $row->find('th')->text;
//      }
//      $name = ($row->find('.service')->count()) ? $row->find('.service')->text : '';
//      $price = ($row->find('.price')->count()) ? $row->find('.price')->text : '';
//      $warranty = ($row->find('.time')->count()) ? $row->find('.time')->text : '';
//
//      if (!$name && !$price && !$warranty) continue;
//
//      $services[$service_name][] = [
//        'name' => $name,
//        'price' => $price,
//        'time' => '',
//        'warranty' => $warranty,
//      ];
//
//    }
//    return $services;
  }

  public function getDescriptionImg($html)
  {
    $result = [
      'logo' => '',
      'description' => '',
    ];

    $dom = new Dom();
    $dom->load($html);

    if ($dom->find('.hero')->count()) {
      $el = new Dom();
      $el = $el->load($dom->find('.hero'));
      $div = $el->find('.top')[0];
      $div->delete();
      $button = $el->find('.button')[0];
      $button->delete();
      $description = $el->innerHtml;
      file_put_contents(public_path() . '/parseImage/gadgets/imac2.png', file_get_contents(self::URL . 'img/heroes/imac2.png'));
      $result = [
        'logo' => '/parseImage/gadgets/imac2.png',
        'description' => $description,
      ];

    }
    if ($dom->find('.hero2')->count()) {
      $el = $dom->find('.hero2');
      $ul = $el->find('ul')[0];
      $ul->delete();
      $div = $el->find('div')[0];
      $div->delete();

      $img = $dom->find('.video')->find('img')[0]->tag->getAttribute('src')['value'];
      $name = explode("/", $img);
      file_put_contents(public_path() . '/parseImage/gadgets/' .end($name) . '.png', file_get_contents(rtrim(self::URL, "/") . $img));
      $description = $el->innerHtml;
      $logo = '/parseImage/gadgets/' .end($name) . '.png';
      $result = [
        'logo' => $logo,
        'description' => $description,
      ];
    }
    if (!$dom->find('.hero')->count() && !$dom->find('.hero2')->count()) {
      $el = $dom->find('.clear');
      $img = $el->find('.preview-right')[0]->tag->getAttribute('src')['value'];

      $div = $el->find('.top')[0];
      if($el->find('.top')->count()){$div->delete();}
      $button = $el->find('.button')[0];
      if($el->find('.button')->count()){$button->delete();}
      $pic = $el->find('.preview-right')[0];
      if($el->find('.preview-right')->count()){$pic->delete();}

      $description = $dom->find('.clear')->innerHtml;

      $name = explode("/", $img);
      file_put_contents(public_path() . '/parseImage/gadgets/' . end($name), file_get_contents(self::URL . $img));
      $result = [
        'logo' => '/parseImage/gadgets/' . end($name),
        'description' => $description,
      ];
    }
    return $result;
  }

}
