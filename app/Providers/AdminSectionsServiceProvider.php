<?php

namespace App\Providers;

use SleepingOwl\Admin\Admin;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

  /**
   * @var array
   */
  protected $sections = [
    \App\Models\User::class => 'App\Http\Sections\Users',
    \App\Models\Category::class => 'App\Http\Sections\Categories',
    \App\Models\Service::class => 'App\Http\Sections\Services',
    \App\Models\Gadget::class => 'App\Http\Sections\Gadgets\Gadgets',
    \App\Models\GadgetServicePivot::class => 'App\Http\Sections\GadgetServicePivots',
//    \App\Models\Slider::class => 'App\Http\Sections\Sliders',
    \App\Models\Work::class => 'App\Http\Sections\Works',
    \App\Models\Module::class => 'App\Http\Sections\Modules',
    \App\Models\CategoryModule::class => 'App\Http\Sections\CategoryModules',
    \App\Models\Contact::class => 'App\Http\Sections\Contacts\Contacts',
    \App\Models\Used::class => 'App\Http\Sections\Useds',
    \App\Models\UsedCategory::class => 'App\Http\Sections\UsedCategories',
    \App\Models\Image::class => 'App\Http\Sections\Images',
    \App\Models\Article::class => 'App\Http\Sections\Articles\Articles',
    \App\Models\Advantage::class => 'App\Http\Sections\Advantages',
    \App\Models\Schedule::class => 'App\Http\Sections\Schedules',
  ];

  protected $child = [
    'gadget-iphone' => [
      'parent' =>  \App\Models\Gadget::class,
      'section' => 'App\Http\Sections\Gadgets\IPhone',
    ],
    'gadget-ipad' => [
      'parent' =>  \App\Models\Gadget::class,
      'section' => 'App\Http\Sections\Gadgets\IPad',
    ],
    'gadget-apple-watch' => [
      'parent' =>   \App\Models\Gadget::class,
      'section' => 'App\Http\Sections\Gadgets\IWatch',
    ],
    'gadget-mac-mini' => [
      'parent' =>   \App\Models\Gadget::class,
      'section' => 'App\Http\Sections\Gadgets\MacMini',
    ],
    'gadget-macbook' => [
      'parent' =>   \App\Models\Gadget::class,
      'section' => 'App\Http\Sections\Gadgets\MacBook',
    ],
    'gadget-imac' => [
      'parent' =>   \App\Models\Gadget::class,
      'section' => 'App\Http\Sections\Gadgets\IMac',
    ],
    'contacts-phone' => [
      'parent' =>  \App\Models\Contact::class,
      'section' => 'App\Http\Sections\Contacts\ContactPhones',
    ],
    'contacts-social' => [
      'parent' =>  \App\Models\Contact::class,
      'section' => 'App\Http\Sections\Contacts\ContactSocials',
    ],
    'contacts-address' => [
      'parent' =>  \App\Models\Contact::class,
      'section' => 'App\Http\Sections\Contacts\ContactAddress',
    ],
    'article-main' => [
      'parent' =>  \App\Models\Article::class,
      'section' => 'App\Http\Sections\Articles\ArticlesMain',
    ],
    'article-other' => [
      'parent' =>  \App\Models\Article::class,
      'section' => 'App\Http\Sections\Articles\ArticlesOther',
    ]
  ];

  /**
   * Register sections.
   *
   * @param Admin $admin
   * @return void
   */
  public function boot(Admin $admin)
  {
    //
    $this->setChildToSection();
    parent::boot($admin);
  }

  private function setChildToSection()
  {
    $parameter = explode('/', request()->getPathInfo())[2] ?? null;

    if ($parameter && in_array($parameter, array_keys($this->child))) {
      try {
        $parent = $this->child[$parameter]['parent'];
        $this->sections[$parent] = $this->child[$parameter]['section'];
      }
      catch (\Exception $e){
      }
    }

  }
}
