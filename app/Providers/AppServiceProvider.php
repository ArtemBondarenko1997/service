<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\CategoryModule;
use App\Models\Gadget;
use App\Models\Module;
use App\Models\Service;
use App\Models\Slider;
use App\Models\Used;
use App\Models\Work;
use App\Observers\CategoryObserver;
use App\Observers\GadgetObserver;
use App\Observers\ModuleObserver;
use App\Observers\ServiceObserver;
use App\Observers\SliderObserver;
use App\Observers\UsedObserver;
use App\Observers\WorkObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      \Schema::defaultStringLength(191);

      Category::observe(CategoryObserver::class);
      Gadget::observe(GadgetObserver::class);
      Slider::observe(SliderObserver::class);
      Work::observe(WorkObserver::class);
      Module::observe(ModuleObserver::class);
      Service::observe(ServiceObserver::class);
      Used::observe(UsedObserver::class);
    }
}
