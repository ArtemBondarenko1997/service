<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {

    Schema::create('images', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('type');
      $table->text('path')->nullable(true);
      $table->timestamps();
    });

    $types = ['logo', 'main_head_left', 'main_head_right'];

    foreach ($types as $type) {
      \App\Models\Image::create(['type' => $type]);
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('images');
  }
}
