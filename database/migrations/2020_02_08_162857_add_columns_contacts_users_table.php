<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsContactsUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->decimal('lat', 8, 6)->nullable(true);
      $table->decimal('lng', 8, 6)->nullable(true);
      $table->string('address')->nullable(true);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('users')) {
      Schema::table('users', function (Blueprint $table) {
        $table->dropColumn('lat');
        $table->dropColumn('lng');
        $table->dropColumn('address');
      });
    }
  }
}
