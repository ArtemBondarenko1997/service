<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCategoryUsedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('useds', function (Blueprint $table) {
        $table->integer('category_id')->nullable(true);
        $table->string('slug')->nullable(true);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('useds', function (Blueprint $table) {
        $table->dropColumn('category_id');
        $table->dropColumn('slug');
      });
    }
}
