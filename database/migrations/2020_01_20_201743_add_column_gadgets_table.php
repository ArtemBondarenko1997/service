<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnGadgetsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('gadgets', function (Blueprint $table) {
      $table->string('icon')->nullable(true);
      $table->string('slug')->nullable(true);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('gadgets')) {
      Schema::table('gadgets', function (Blueprint $table) {
        $table->dropColumn('icon');
        $table->dropColumn('slug');
      });
    }
  }
}
