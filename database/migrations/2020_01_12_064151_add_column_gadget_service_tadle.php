<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnGadgetServiceTadle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('gadget_service', function (Blueprint $table) {
        $table->integer('order')->nullable(true);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      if (Schema::hasTable('gadget_service')) {
        Schema::table('gadget_service', function (Blueprint $table) {
          $table->dropColumn('order');
        });
      }
    }
}
