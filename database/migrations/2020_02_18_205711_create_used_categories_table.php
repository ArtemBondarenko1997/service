<?php

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use SleepingOwl\Admin\Exceptions\RepositoryException;

class CreateUsedCategoriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   * @throws BindingResolutionException
   * @throws RepositoryException
   */
  public function up()
  {
    Schema::create('used_categories', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('name')->unique();
      $table->string('slug')->nullable(true);
      $table->timestamps();
    });

    $data = [
      [
        'name' => 'Девайсы',
        'slug' => \Illuminate\Support\Str::slug('Девайсы', '-', 'en')
      ],
      [
        'name' => 'Аксессуары',
        'slug' => \Illuminate\Support\Str::slug('Аксессуары', '-', 'en')
      ]
    ];

    \DB::table('used_categories')->insert($data);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('used_categories');
  }
}
