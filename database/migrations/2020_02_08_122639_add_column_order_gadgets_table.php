<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOrderGadgetsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('gadgets', function (Blueprint $table) {
      $table->integer('order')->nullable(true);
      $table->boolean('is_publish')->nullable(true);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    if (Schema::hasTable('gadgets')) {
      Schema::table('gadgets', function (Blueprint $table) {
        $table->dropColumn('order');
        $table->dropColumn('is_publish');
      });
    }
  }
}
