<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('schedules', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->integer('day');
        $table->string('work');
        $table->timestamps();
      });


      for ($i = 1; $i <= 7; $i++) {

        if ($i == 7) {
          \App\Models\Schedule::create([
            'day' => $i,
            'work' => 'Выходной',
          ]);
        } else {
          \App\Models\Schedule::create([
            'day' => $i,
            'work' => '10:00 - 19:00',
          ]);
        }

      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
