<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('contacts', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('sys_type');
      $table->string('name');
      $table->string('value');
      $table->boolean('is_publish');
      $table->integer('order')->nullable(true);
      $table->timestamps();
    });

    $data = [
      [
        'sys_type' => 'phone',
        'name' => 'kyivstar',
        'value' => '096 656 7027',
        'is_publish' => true,
      ],
      [
        'sys_type' => 'phone',
        'name' => 'mts',
        'value' => '095 220 2083',
        'is_publish' => true,
      ],
      [
        'sys_type' => 'phone',
        'name' => 'life',
        'value' => '063 596 64 55',
        'is_publish' => true,
      ],
      [
        'sys_type' => 'social',
        'name' => 'instagram',
        'value' => 'https://www.instagram.com/apple_android_zhurbenko_ukrain/',
        'is_publish' => true,
      ],
      [
        'sys_type' => 'social',
        'name' => 'facebook',
        'value' => 'https://www.facebook.com/people/%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9-%D0%96%D1%83%D1%80%D0%B1%D0%B5%D0%BD%D0%BA%D0%BE/100024947829148',
        'is_publish' => true,
      ],
      [
        'sys_type' => 'social',
        'name' => 'skype',
        'value' => 'skype',
        'is_publish' => false,
      ],
      [
        'sys_type' => 'social',
        'name' => 'viber',
        'value' => 'viber',
        'is_publish' => false,
      ],
      [
        'sys_type' => 'social',
        'name' => 'telegram',
        'value' => 'telegram',
        'is_publish' => false,
      ],
      [
        'sys_type' => 'social',
        'name' => 'whatsapp',
        'value' => 'whatsapp',
        'is_publish' => false,
      ]
    ];

    \DB::table('contacts')->insert($data);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('contacts');
  }
}
