<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGadgetServiceTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('gadget_service', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('gadget_id');
      $table->integer('service_id');
      $table->integer('old_price')->nullable(true);
      $table->integer('price');
      $table->boolean('is_from')->default(false);
      $table->string('time')->nullable(true);
      $table->integer('parent_id')->nullable(true);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('gadget_service');
  }
}
