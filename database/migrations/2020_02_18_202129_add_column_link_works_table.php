<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLinkWorksTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('works', function (Blueprint $table) {
      $table->string('gadget_slug')->nullable(true);
      $table->string('service')->nullable(true);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('works', function (Blueprint $table) {
      $table->dropColumn('gadget_slug');
      $table->dropColumn('service');
    });
  }
}
