<?php

use Carbon\Carbon;
use App\Services\FileServices;
use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image as ImageInt;
use Illuminate\Support\Facades\File;

class CategoriesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $prefix = 'parseImage/categories/';
    $path = 'images/' . \App\Models\Category::FOLDER . '/';

    if (!File::exists(public_path($path))) {
      File::makeDirectory(public_path($path), 0777, true, true);
    }
    $categories = [
      [
        'name' => 'iPhone',
        'icon' => 'iphone.png',
        'logo' => 'iphone-logo.png',
        'slug' => 'iphone',
        'order' => 1,
      ],
      [
        'name' => 'iPad',
        'icon' => 'ipad-big.png',
        'logo' => 'ipad-logo.png',
        'slug' => 'ipad',
        'order' => 2,
      ],
      [
        'name' => 'iMac',
        'icon' => 'imac-big.png',
        'logo' => 'imac-logo.png',
        'slug' => 'imac',
        'order' => 5,
      ],
      [
        'name' => 'Apple Watch',
        'icon' => 'watch-big.png',
        'logo' => 'watch-logo.png',
        'slug' => 'watch',
        'order' => 6,
      ],
      [
        'name' => 'MacBook',
        'icon' => 'macbook-big.png',
        'logo' => 'macbook-logo.png',
        'slug' => 'macbook',
        'order' => 3,
      ],
      [
        'name' => 'Mac Mini',
        'icon' => 'mac-mini-big.png',
        'logo' => 'mac-mini-logo.png',
        'slug' => 'mac-mini',
        'order' => 4,
      ],
    ];

    foreach ($categories as $category){

      $icon = ImageInt::make(public_path($prefix) . '/' .$category['icon']);
      $iconName = FileServices::generateFileName($icon, $icon->basename, \App\Models\Category::FOLDER);
      $icon->save(public_path($path . $iconName));

      $logo = ImageInt::make(public_path($prefix) . '/' .$category['logo']);
      $logoName = FileServices::generateFileName($logo, $logo->basename, \App\Models\Category::FOLDER);
      $logo->save(public_path($path . $logoName));

      $model = new \App\Models\Category();
      $model->name = $category['name'];
      $model->icon = $path . $iconName;
      $model->logo = $path . $logoName;
      $model->slug = $category['slug'];
      $model->order = $category['order'];

      $model->save();
    }
  }
}
