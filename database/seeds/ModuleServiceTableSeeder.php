<?php

use Illuminate\Database\Seeder;

class ModuleServiceTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      \App\Models\CategoryModule::query()->delete();
        $categories = \App\Models\Category::all();
        $modules = \App\Models\Module::all()->pluck('id');
        $services = \App\Models\Service::whereNotNull('parent_id')->get()->pluck('id');

        foreach ($categories as $category){
          for($i = 0; $i < 10; $i++){
            $model = new \App\Models\CategoryModule();
            $model->create([
              'category_id' => $category->id,
              'module_id' => $modules[rand(0, count($modules) - 1)],
              'service_id' => $services[rand(0, count($services) - 1)],
            ]);
          }
        }

    }
}
