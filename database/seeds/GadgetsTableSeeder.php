<?php

use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image as ImageInt;

class GadgetsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    require('files/gadgets.php');
    require('files/services.php');

    $services = array_keys($servicesKeyValue);
    $servicesModel = \App\Models\Service::whereNotNull('parent_id')->get()->pluck('id', 'name');
    $categories = \App\Models\Category::all()->pluck('id', 'name');
    $path = public_path() . '/images/logos/';

    if(!File::exists($path)) {
      File::makeDirectory($path, 0777, true, true);
    }

    foreach ($gadgets as $category => $gadget) {
      foreach ($gadget as $fields) {

        if($fields['logo']){
          $filename = Str::random(20) . '.png';
//          $img = ImageInt::make($fields['logo']);
//          $img->save($path . $filename);
        }

        $model = new \App\Models\Gadget();
        $model->category_id = $categories[$category];
        $model->model = str_replace('Ремонт ', '', $fields['title']);
        $model->title = $fields['title'];
        $model->description = $fields['description'];
        $model->logo = '/images/logos/' . $filename;
        $model->is_publish = true;
        $model->save();

        foreach ($fields['services'] as $pivot) {
          $parentName = $servicesKeyValue[$pivot['service']];
          $parentId = \App\Models\Service::where('name', '=', $parentName)->first();

          $parentPivot = \App\Models\GadgetServicePivot::where('service_id', '=', $parentId->id)->where('gadget_id', '=', $model->id)->first();

          if($parentPivot === null){
            $parentPivot = new \App\Models\GadgetServicePivot();
            $parentPivot->gadget_id = $model->id;
            $parentPivot->service_id = $parentId->id;
            $parentPivot->price = 0;
            $parentPivot->save();
          }else{
            $childrenPivot = new \App\Models\GadgetServicePivot();
            $childrenPivot->gadget_id = $model->id;
            $childrenPivot->service_id = $servicesModel[$pivot['service']];
            $childrenPivot->price = $this->getPrice($pivot['price']);
            $childrenPivot->time = $pivot['time'];
            $childrenPivot->is_from =  strpos($pivot['price'], 'от') !== false;
            $childrenPivot->parent_id = $parentPivot->id ;
            $childrenPivot->save();
          }

        }
      }
    }

  }

  /**
   * @param $price
   * @return int
   */
  private function getPrice($price)
  {
    $res = $price;
    if(strpos($price, '|') ){
      $pos = strpos($price, '|');
      $res = substr_replace($price, '', $pos);
    }
    $res = str_replace('от', '', $price);
    $res = str_replace('грн.', '', $res);
    $res = str_replace('грн', '', $res);
    $res = trim($res);
    return (int)$res;
  }
}
