<?php

use App\Models\Module;
use App\Services\FileServices;
use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image as ImageInt;
use Illuminate\Support\Facades\File;

class ModulesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $path = 'images/' . \App\Models\Module::FOLDER . '/';
    $prefix = 'parseImage/modules/';

    if (!File::exists(public_path($path))) {
      File::makeDirectory(public_path($path), 0777, true, true);
    }

    $modules = [
      ['name' => 'Экран', 'icon' => 'screen.png'],
      ['name' => 'Стекло', 'icon' => 'screen.png'],
      ['name' => 'Корпус', 'icon' => 'case.png'],
      ['name' => 'Крышка', 'icon' => 'case.png'],
      ['name' => 'WI-FI', 'icon' => 'wifi.png'],
      ['name' => 'Батарея', 'icon' => 'power.png'],
      ['name' => 'Динамик', 'icon' => 'sound.png'],
      ['name' => 'Кнопки', 'icon' => 'buttons.png'],
      ['name' => 'Микрофон', 'icon' => 'microphone.png'],
      ['name' => 'Камера', 'icon' => 'camera.png'],
      ['name' => 'Данные', 'icon' => 'data.png'],
      ['name' => 'iCloud', 'icon' => 'data.png'],
      ['name' => 'Home', 'icon' => 'home.png'],
      ['name' => 'Материнская плата', 'icon' => 'motherboard.png'],
      ['name' => 'Залитие', 'icon' => 'water.png'],
      ['name' => 'Не включается', 'icon' => 'not-on.png'],
      ['name' => 'Не заряжается', 'icon' => 'not-charge.png'],
      ['name' => 'Не видит сеть', 'icon' => 'not-connect.png'],
      ['name' => 'Вирусы', 'icon' => 'virus.png'],
      ['name' => 'Нет звука', 'icon' => 'not-sound.png'],
      ['name' => 'Шумит', 'icon' => 'noise.png'],
      ['name' => 'Грееется', 'icon' => 'hot.png'],
      ['name' => 'Клавиатура', 'icon' => 'keyboard.png'],
      ['name' => 'Bluetooth', 'icon' => 'bluetooth.png'],
      ['name' => 'Вибро мотор', 'icon' => 'vibro.png'],
    ];

    foreach ($modules as $module){

      $file = ImageInt::make(public_path($prefix) . '/' .$module['icon']);
      $fileName = FileServices::generateFileName($file, $file->basename, Module::FOLDER);
      $file->save(public_path($path . $fileName));

      $model = new \App\Models\Module();
      $model->name = $module['name'];
      $model->icon = $path . $fileName;
      $model->save();

    }
  }
}
