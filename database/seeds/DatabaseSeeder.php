<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(CategoriesTableSeeder::class);
         $this->call(ServicesTableSeeder::class);
         $this->call(GadgetsTableSeeder::class);
         $this->call(ModulesTableSeeder::class);
         $this->call(UsedsTableSeeder::class);
         $this->call(ModuleServiceTableSeeder::class);
    }
}
