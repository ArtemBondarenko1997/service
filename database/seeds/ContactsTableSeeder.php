<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
          'sys_type' => 'address',
          'name' => 'г. Днепр',
          'value' => ' ул Вячеслава Липинскогоб 9',
          'is_publish' => true,
        ];

        \App\Models\Contact::create($data);
    }
}
