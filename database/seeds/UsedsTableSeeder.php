<?php

use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image as ImageInt;
use Illuminate\Support\Facades\File;

class UsedsTableSeeder extends Seeder
{
  const COUNT_PRODUCTS = 20;

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker\Factory::create();
    $category_id = $this->createCategory();

    \App\Models\Used::query()->delete();
    for ($i = 0; $i < self::COUNT_PRODUCTS; $i++) {

      $name = $faker->text(20);

      $model = new \App\Models\Used();
      $model->name = $faker->text(20);
      $model->slug = Str::slug($name, '-', 'en');
      $model->price = rand(100, 30000);
      $model->description = $faker->text('150');
      $model->category_id = $category_id[rand(0, 1)];
      $model->save();

      $this->savePhoto($model);
    }
  }

  public function createCategory()
  {
    \App\Models\UsedCategory::query()->delete();

    $id = [];
    $data = ['Девайсы', 'Аксессуары'];

    foreach ($data as $name) {
      $model = new \App\Models\UsedCategory();
      $model->name = $name;
      $model->save();
      $id[] = $model->id;
    }
    return $id;
  }

  public function savePhoto(\App\Models\Used $model)
  {
    $filePath = '/images/useds/' . $model->id . '/';

    if (!File::exists(public_path($filePath), 0777, true, true)) {
      File::makeDirectory(public_path($filePath), 0777, true, true);
    }

    for($i = 1; $i <= 3; $i++){
      $fileName = Str::random(20) . '.jpg';
      $photo = ImageInt::make(public_path('parseImage/useds/'. $i .'.jpg'))
        ->save(public_path($filePath) . '/' . $fileName);

      $model->photos()->create([
        'name' => $fileName,
        'order' => $i,
        'is_main' => $i === 1,
      ]);
    }
    return $model->photos();
  }
}
