<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/admin/gadget-dependent', 'Api\SelectController@gadgetDependent');

Route::get('/categories', 'Api\CategoryController@getCategories');
Route::get('/category/{slug}', 'Api\CategoryController@getGadgets');
Route::get('/category/gadget/{slug}', 'Api\CategoryController@getGadget')->name('gadget');

Route::get('/slider', 'Api\SliderController@all')->name('sliders');

Route::get('/contacts', 'Api\ContactController@all')->name('contacts');

Route::get('/modules/{category}', 'Api\ModuleController@modules')->name('modules');
Route::get('/module/{category}/{service}', 'Api\ModuleController@module')->name('module');

Route::get('/useds/{category?}', 'Api\UsedController@useds')->name('useds');

Route::get('/work', 'Api\WorkController@works')->name('works');

Route::get('/products/{category?}', 'Api\ProductsController@products')->name('products');
Route::get('/product/{slug}', 'Api\ProductsController@product')->name('product');

Route::get('/articles', 'Api\ArticleController@get');
Route::get('/articles/{page}', 'Api\ArticleController@getByPage');
Route::post('/order', 'Api\OrderController@makeOrder')->name('order');

Route::get('/schedules', 'Api\ScheduleController@get');

Route::get('/advantages', 'Api\AdvantageController@get');
Route::get('/advantages/{advantage}', 'Api\AdvantageController@single');
