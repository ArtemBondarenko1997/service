let mix = require('laravel-mix');

mix.setPublicPath('../../../../public/packages/sleepingowl/default');
mix.sass('scss/app.scss', 'css/admin-app.css')
  .js('js_owl/app.js', 'js/admin-app.js')
  .js('js_owl/vue_init.js', 'js/vue.js')
  .js('js_owl/modules_load.js', 'js/modules.js')

  .options({
    processCssUrls: true,
    resourceRoot: '../',
    imgLoaderOptions: {
      enabled: false,
    },
  });
